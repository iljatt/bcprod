
FactorOut[1] = 1;  
FactorOut[Longest[c_] exp_, p_] /; FreeQ[c, p] := c FactorOut[exp];
FactSumOut[c_, p_] /; FreeQ[c, p] := MISS[c]; 
FactSumOut[ Longest[c_] + exp_, p_] /; FreeQ[c, p] := MISS[c] + MATCH[exp]; 
FactSumOut[exp_, p_] /; ! FreeQ[exp, p] := MATCH[exp];
(*Protect[FactorOut];*)


ColorSpin[s___] := Module[{x = List[s], n, prefactor},
   prefactor = Power[GS, Count[x, GS]];
   x = Delete[x, Position[x, GS][[ ;; ]]];
   n = Count[x, _SUNT]; 
   If[n == Length[x], x = CDot @@ x,
    If[n > 0, x = CDot @@ x[[;; n]] SDot @@ x[[n + 1 ;;]], 
     x = SDot @@ x ]];
   Return[prefactor x];
   ];


DoColorSpin[exp_] := Module[{tmp},
   tmp = Trick[exp];
   tmp = tmp /. x_Dot :> Trick[Distribute[x]];
   tmp = tmp /. x_Dot :> Distribute[x];
   tmp = tmp /. Dot -> ColorSpin; 
   If[FreeQ[tmp, CDot], 
    tmp = FactorOut[tmp, _SUNT] /. FactorOut -> CDot]; 
   tmp = Collect[tmp, _CDot, Factor]; 
   (*tmp=tmp/.CDot[t___] :> SUNTrace[Dot[t]];   if no trace *)
   (*tmp=Collect[tmp,_SDot, SUNSimplify]; (*Collect2 as well*)*)
   Return[tmp];
   ];


ReOrder[exp__] := 
  Module[{tmp = List[exp], sgn = 1, xfv, xdv, ch, i}, 
   xfv = Position[tmp, DiracGamma[_]];
   xdv = Position[tmp, DiracGamma[_, _]]; (*Min[empty array] = inf,  Max[empty array] = -inf*)
   While[Min[xdv] < Max[xfv],
    For[i = 1, i < Length[tmp], i++,
      If[MemberQ[xdv, {i}] && MemberQ[xfv, {i + 1}], 
       ch = tmp[[i]]; tmp[[i]] = tmp[[i + 1]]; tmp[[i + 1]] = ch;
       sgn = -sgn; 
       ];
      xfv = Position[tmp, DiracGamma[_]];
      xdv = Position[tmp, DiracGamma[_, _]];
      ];
    ];
   tmp = Insert[tmp, sgn, 1];
   Return[Dot @@ tmp];
   ];


EpsSplit[exp__] := Module[{x = List[exp] , N, L, reg, sing, sgn},
   sgn = x[[1]]; 
   x = Delete[x, 1];
   N = Count[x, DiracGamma[_, D - 4]];
   L = Length[x];
   If[N < L, reg = Dot @@ x[[;; L - N]], reg = Null];
   If[N > 0, sing = Dot @@ x[[L - N + 1 ;;]], sing = Null];
   Return[{sgn, reg, sing}];
   ];


THoFactor[exp_] := Module[{tmp}, 
   tmp = exp /. 
     DiracGamma[li_] :> DiracGamma[li] + ChangeDimension[DiracGamma[li],D-4];
   tmp = tmp /. $CC[list__] :> Distribute@$CC[Distribute@Dot[list]];
   tmp = tmp /. $BB[list__] :> Distribute@$BB[Distribute@Dot[list]];
   tmp = tmp /. SDot[list__] :> Distribute@SDot[Distribute@Dot[list]];
   tmp = tmp /. Dot -> ReOrder;
   tmp = tmp /. Dot -> EpsSplit;
   tmp = tmp /. $CC[{s_, x_, y_}] :> s $CC[x] $EPS[y];
   tmp = tmp /. $BB[{s_, x_, y_}] :> s $BB[x] $EPS[y];
   tmp = tmp /. SDot[{s_, x_, y_}] :> s DiracTrace[x] $EPS[y];
   tmp = tmp /. {$EPS[Null] -> 1, $BB[Null] -> 1, $CC[Null] -> 1, DiracTrace[Null] -> 1};
   tmp = tmp /. {$CC[DiracGamma[x_, D-4]] :> $EPS[DiracGamma[x, D-4]]};
   tmp = tmp /. {$BB[DiracGamma[y_, D-4]] :> $EPS[DiracGamma[y, D-4]]};
   tmp = tmp /. DiracTrace -> Tr;
   Return[tmp];
   ];


THoEvaluate[exp_, intMom_] := Module[{tmp},
   (*All the D-4 vectors are already inside $EPS. /4 --- /Tr[1]. *)
   tmp = exp /. D -> (D + 4);
   tmp = tmp /. $EPS[ex_] :> $EPS[DiracTrace[ex]];
   tmp = tmp /. $EPS[DiracTrace[ex_]] :> MomentumExpand@Tr[ex]/4;
   tmp = tmp /. D -> (D - 4); 
   tmp = tmp /. Momentum[mom_, D - 4] /; ! SameQ[mom, intMom] -> 0 ;
   tmp = tmp /. Pair[LorentzIndex[_, D - 4], Momentum[intMom, D - 4]] -> 0;
   tmp = tmp /. Pair[LorentzIndex[_, D - 4], LorentzIndex[_, D - 4]] -> 0;
   tmp = tmp /. Dot -> List;(*not necessary*)
   tmp = tmp /. {$BB[List[x__]] :> $BB[x], $CC[List[x__]] :> $CC[x]};
   Return[tmp];
   ];


DoContract[exp_] := Module[{tmp},   
   tmp = FactorOut[exp, _LorentzIndex];
   tmp = tmp /. FactorOut[x_] :> 
         Distribute[$VF[ExpandAll[x, p_ /; ! FreeQ[p, _LorentzIndex]]]];           
   tmp = tmp /. $VF[x_] :> Contract[x];
   Return[tmp];
   ];


RefineGammaChain[exp_] := Module[{tmp},
   tmp = exp /. $BB[x__] /; ! DuplicateFreeQ[List[x]] :> $BB[DiracTrick@Dot[x]];
   tmp = tmp /. $CC[y__] /; ! DuplicateFreeQ[List[y]] :> $CC[DiracTrick@Dot[y]];
   tmp = tmp /. $BB[x_] :> Distribute@$BB[Distribute[x]]; 
   tmp = tmp /. $CC[y_] :> Distribute@$CC[Distribute[y]];
   tmp = tmp /. $BB[Longest[c_] gammachain_] /; 
   		FreeQ[c, DiracGamma] :> c $BB[gammachain];
   tmp = tmp /. $CC[Longest[c_] gammachain_] /; 
       FreeQ[c, DiracGamma] :> c $CC[gammachain];
   tmp = tmp /. $BB[c_] /; FreeQ[c, DiracGamma] :> c $BB[];
   tmp = tmp /. $CC[c_] /; FreeQ[c, DiracGamma] :> c $CC[];
   tmp = tmp /. Dot -> List;(*not necessary*)
   tmp = tmp /. {$BB[List[x__]] :> $BB[x], $CC[List[x__]] :> $CC[x]};
   Return[tmp];
   ];


ReWrite4GluonVertex[exp_] := Module[{tmp = exp, copy, x},
   If[! FreeQ[tmp, SUNF[_, _, _, _]],
    tmp = FactorOut[tmp, SUNF[_, _, _, _]];  
    copy = tmp /. FactorOut[__] -> 1; 
    tmp =  tmp /. FactorOut[ex_] :>  Distribute@$CP[Collect2[Expand[ex], _SUNF]];
    x = Cases2[tmp, $CP]; 
    tmp = x copy /. $CP[ex_] :> ex;
    ];
   Return[tmp];
   ];


IntMomPick[exp__, intMom_] := Module[{x = List[exp] , flag, y},
   (*flag = !FreeQ2[#,intMom]&/@ x;
      Print["BEFORE","\t",x,"\t",flag];*)
   y = DiracGammaExpand[#] & /@ x;
   y = FactSumOut[#, intMom] & /@ y;
   y = y /. MISS -> DiracGammaCombine /. MATCH[ex_] :> ex;
   (*Print["AFTER","\t",y];*)
   Return[y];
   ];


IntMomArrange[exp_, intMom_] := Module[{tmp},
   (*spinor lines evaluation*)
   tmp = exp /. $CC[x__] :> $CC[IntMomPick[x, intMom]];
   tmp = tmp /. $BB[x__] :> $BB[IntMomPick[x, intMom]];
   tmp = tmp /. $BB[List[x__]] :>  
      Distribute@$BB[Distribute[Dot[x]]];
   tmp = tmp /. $CC[List[x__]] :> Distribute@$CC[Distribute[Dot[x]]];
   (*FourVector evaluation*)
   tmp = tmp /. Pair[LorentzIndex[li_], Momentum[p_]] :> $FV[
       Pair[LorentzIndex[li], Momentum[p]]];
   tmp = tmp /. $FV[vec_] :> $FV[
       FactSumOut[ExpandScalarProduct@Simplify[vec], intMom]];
   tmp = tmp /. MISS -> MomentumCombine /. MATCH[ex_] :> ex;
   tmp = tmp /. $FV[vec_] :> vec;
   (*ScalarProduct evaluation*)
   tmp = tmp /. Pair[Momentum[p_], Momentum[q_]] :> $SP[
   			Pair[Momentum[p], Momentum[q]]];
   tmp = tmp /. $SP[prod_] :> $SP[
   	 		FactSumOut[ExpandScalarProduct[prod], intMom]];
   tmp = tmp /. MISS -> MomentumCombine /. MATCH -> MomentumCombine;
   tmp = tmp /. $SP[prod_] :> prod;
   (*final expanding in lorentz part*)
   tmp = FactorOut[tmp, _LorentzIndex];
   tmp = tmp /. FactorOut -> Expand;
   tmp = tmp /. Dot -> List;
   tmp = tmp /. {$BB[List[x__]] :> $BB[x], $CC[List[x__]] :> $CC[x]};
   Return[tmp];
   ];

