{I*GS^2*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*MetricTensor[li5, li6]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-qc - qcbar, 0]*
  (-(MetricTensor[li1, li6]*MetricTensor[li2, li4]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
       Index[Gluon, 8]] + SUNF[Index[Gluon, 1], Index[Gluon, 7], 
       Index[Gluon, 2], Index[Gluon, 8]])) + MetricTensor[li1, li2]*
    MetricTensor[li4, li6]*(SUNF[Index[Gluon, 1], Index[Gluon, 7], 
      Index[Gluon, 2], Index[Gluon, 8]] - SUNF[Index[Gluon, 1], 
      Index[Gluon, 8], Index[Gluon, 7], Index[Gluon, 2]]) + 
   MetricTensor[li1, li4]*MetricTensor[li2, li6]*
    (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
      Index[Gluon, 8]] + SUNF[Index[Gluon, 1], Index[Gluon, 8], 
      Index[Gluon, 7], Index[Gluon, 2]])), 
 I*GS^2*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*(FourVector[-p1 + p2, li3]*
    MetricTensor[li1, li2] + FourVector[p1 + qb + qbbar + qc + qcbar, li2]*
    MetricTensor[li1, li3] + FourVector[-p2 - qb - qbbar - qc - qcbar, li1]*
    MetricTensor[li2, li3])*MetricTensor[li3, li4]*MetricTensor[li5, li6]*
  (FourVector[-2*qb - 2*qbbar - qc - qcbar, li8]*MetricTensor[li4, li6] + 
   FourVector[qb + qbbar + 2*(qc + qcbar), li6]*MetricTensor[li4, li8] + 
   FourVector[qb + qbbar - qc - qcbar, li4]*MetricTensor[li6, li8])*
  MetricTensor[li7, li8]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 7]]*SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 (-I)*GS*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[-qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li3]*MetricTensor[li1, li2] + 
   FourVector[p1 + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li3] + 
   FourVector[-p2 - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li3])*
  MetricTensor[li3, li4]*MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[qb + qbbar + qcbar, MC]*
  PropagatorDenominator[qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7]], 
 (-I)*GS*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li3]*MetricTensor[li1, li2] + 
   FourVector[p1 + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li3] + 
   FourVector[-p2 - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li3])*
  MetricTensor[li3, li4]*MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qb - qbbar - qc, MC]*PropagatorDenominator[
   qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 7]], (-I)*GS*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[-qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 + p2, li3]*MetricTensor[li1, li2] + 
   FourVector[p1 + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li3] + 
   FourVector[-p2 - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li3])*
  MetricTensor[li3, li4]*MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qc + qcbar, 0]*
  PropagatorDenominator[qbbar + qc + qcbar, MB]*
  PropagatorDenominator[qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7]], 
 (-I)*GS*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 + p2, li3]*MetricTensor[li1, li2] + 
   FourVector[p1 + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li3] + 
   FourVector[-p2 - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li3])*
  MetricTensor[li3, li4]*MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qc - qcbar, MB]*
  PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
   qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 7]], I*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[p2 - qbbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li3, li4]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-p2 + qbbar, MB]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   -p2 + qbbar + qc + qcbar, MB], 
 I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[-p2 + qc + qcbar, 
   0]*PropagatorDenominator[-p2 + qbbar + qc + qcbar, MB], 
 I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
   -p2 + qc + qcbar, 0]*PropagatorDenominator[-p2 + qbbar + qc + qcbar, MB], 
 I*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
   qbbar + qc + qcbar, MB]*PropagatorDenominator[-p2 + qbbar + qc + qcbar, 
   MB], (-I)*GS*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li3, li4]*
  (FourVector[-2*p2 + qc + qcbar, li5]*MetricTensor[li2, li3] + 
   FourVector[p2 + qc + qcbar, li3]*MetricTensor[li2, li5] + 
   FourVector[p2 - 2*(qc + qcbar), li2]*MetricTensor[li3, li5])*
  MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qc - qcbar, 0]*
  PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
   -p2 + qbbar + qc + qcbar, MB]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], I*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-p2 + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li3, li4]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb, MB]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   p2 - qb - qc - qcbar, MB], 
 I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
   0]*PropagatorDenominator[-p2 + qb + qbbar + qcbar, MC], 
 I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
   0]*PropagatorDenominator[p2 - qb - qbbar - qc, MC], 
 I*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[-qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[qc + qcbar, 0]*
  PropagatorDenominator[qbbar + qc + qcbar, MB], 
 (-I)*GS*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li3, li4]*
  (FourVector[-p1 + p2 - qb - qbbar, li5]*MetricTensor[li1, li3] + 
   FourVector[p1 + qc + qcbar, li3]*MetricTensor[li1, li5] + 
   FourVector[-p2 + qb + qbbar - qc - qcbar, li1]*MetricTensor[li3, li5])*
  MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb, MB]*
  PropagatorDenominator[-p2 + qb + qbbar, 0]*PropagatorDenominator[
   qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]], 
 I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[
   p2 - qb - qc - qcbar, MB]*PropagatorDenominator[-p2 + qc + qcbar, 0], 
 I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb - qc - qcbar, MB]*
  PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
   -p2 + qc + qcbar, 0], I*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li3, li4]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qc - qcbar, MB]*
  PropagatorDenominator[p2 - qb - qc - qcbar, MB]*
  PropagatorDenominator[qc + qcbar, 0], 
 (-I)*GS*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li3, li4]*
  (FourVector[-2*p2 + qc + qcbar, li5]*MetricTensor[li2, li3] + 
   FourVector[p2 + qc + qcbar, li3]*MetricTensor[li2, li5] + 
   FourVector[p2 - 2*(qc + qcbar), li2]*MetricTensor[li3, li5])*
  MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qc - qcbar, 0]*
  PropagatorDenominator[p2 - qb - qc - qcbar, MB]*
  PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[p2 - qbbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
   -p2 + qbbar, MB]*PropagatorDenominator[-p2 + qb + qbbar + qcbar, MC], 
 I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
   -p2 + qbbar, MB]*PropagatorDenominator[p2 - qb - qbbar - qc, MC], 
 I*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
   -qb - qc - qcbar, MB]*PropagatorDenominator[qc + qcbar, 0], 
 (-I)*GS*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li3, li4]*
  (FourVector[-p1 + p2 - qb - qbbar, li5]*MetricTensor[li1, li3] + 
   FourVector[p1 + qc + qcbar, li3]*MetricTensor[li1, li5] + 
   FourVector[-p2 + qb + qbbar - qc - qcbar, li1]*MetricTensor[li3, li5])*
  MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-p2 + qbbar, MB]*
  PropagatorDenominator[-p2 + qb + qbbar, 0]*PropagatorDenominator[
   qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]], 
 I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-p2 + qcbar, 
   MC]*PropagatorDenominator[-p2 + qb + qbbar + qcbar, MC], 
 I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
   qb + qbbar + qcbar, MC]*PropagatorDenominator[-p2 + qb + qbbar + qcbar, 
   MC], (-I)*GS*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  (FourVector[-p2 - qb - qbbar, li5]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - qb - qbbar, li3]*MetricTensor[li2, li5] + 
   FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li3, li5])*
  MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[
   -p2 + qb + qbbar + qcbar, MC]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-p2 + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li3, li4]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[
   p2 - qb - qbbar - qc, MC], 
 I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[-qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
  PropagatorDenominator[qb + qbbar + qcbar, MC], 
 (-I)*GS*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  (FourVector[-p1 - qb - qbbar, li5]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qc + qcbar, li3]*MetricTensor[li1, li5] + 
   FourVector[p2 + qb + qbbar - qc - qcbar, li1]*MetricTensor[li3, li5])*
  MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qb + qbbar, 0]*
  PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[-p2 + qc + qcbar, 
   0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]], 
 I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
   -qb - qbbar - qc, MC]*PropagatorDenominator[p2 - qb - qbbar - qc, MC], 
 (-I)*GS*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  (FourVector[-p2 - qb - qbbar, li5]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - qb - qbbar, li3]*MetricTensor[li2, li5] + 
   FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li3, li5])*
  MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[
   p2 - qb - qbbar - qc, MC]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], I*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li3, li4]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qb - qbbar - qc, MC]*PropagatorDenominator[
   -p2 + qcbar, MC], (-I)*GS*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  (FourVector[-p1 - qb - qbbar, li5]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qc + qcbar, li3]*MetricTensor[li1, li5] + 
   FourVector[p2 + qb + qbbar - qc - qcbar, li1]*MetricTensor[li3, li5])*
  MetricTensor[li5, li6]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qb + qbbar, 0]*
  PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
   -p2 + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], I*GS^2*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  (FourVector[-p1 + p2 - qc - qcbar, li5]*MetricTensor[li1, li3] + 
   FourVector[p1 + qb + qbbar, li3]*MetricTensor[li1, li5] + 
   FourVector[-p2 - qb - qbbar + qc + qcbar, li1]*MetricTensor[li3, li5])*
  (FourVector[-2*p2 + qc + qcbar, li7]*MetricTensor[li2, li4] + 
   FourVector[p2 + qc + qcbar, li4]*MetricTensor[li2, li7] + 
   FourVector[p2 - 2*(qc + qcbar), li2]*MetricTensor[li4, li7])*
  MetricTensor[li5, li6]*MetricTensor[li7, li8]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qb + qbbar, 0]*
  PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
   -p2 + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]]*SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 9]], 
 I*GS^2*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li8]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li3, li4]*
  (FourVector[-p1 + p2 - qb - qbbar, li5]*MetricTensor[li1, li3] + 
   FourVector[p1 + qc + qcbar, li3]*MetricTensor[li1, li5] + 
   FourVector[-p2 + qb + qbbar - qc - qcbar, li1]*MetricTensor[li3, li5])*
  (FourVector[-2*p2 + qb + qbbar, li7]*MetricTensor[li2, li4] + 
   FourVector[p2 + qb + qbbar, li4]*MetricTensor[li2, li7] + 
   FourVector[p2 - 2*(qb + qbbar), li2]*MetricTensor[li4, li7])*
  MetricTensor[li5, li6]*MetricTensor[li7, li8]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qb + qbbar, 0]*
  PropagatorDenominator[-p2 + qb + qbbar, 0]*PropagatorDenominator[
   qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]]*
  SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 9]]}
