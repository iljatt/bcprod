{GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qcbar, MC]*(FourVector[-p1 + p2, li4]*
    MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
     li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-qc - qcbar, 0]*
  PropagatorDenominator[q + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]]*
  (-(MetricTensor[li3, li9]*MetricTensor[li5, li7]*
     (SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9], 
       Index[Gluon, 10]] + SUNF[Index[Gluon, 7], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 10]])) + MetricTensor[li3, li5]*
    MetricTensor[li7, li9]*(SUNF[Index[Gluon, 7], Index[Gluon, 9], 
      Index[Gluon, 8], Index[Gluon, 10]] - SUNF[Index[Gluon, 7], 
      Index[Gluon, 10], Index[Gluon, 9], Index[Gluon, 8]]) + 
   MetricTensor[li3, li7]*MetricTensor[li5, li9]*
    (SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9], 
      Index[Gluon, 10]] + SUNF[Index[Gluon, 7], Index[Gluon, 10], 
      Index[Gluon, 9], Index[Gluon, 8]])), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar - qc - 
       qcbar]) . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    -p2 + q + qc + qcbar, 0]*PropagatorDenominator[
    -p2 + q + qbbar + qc + qcbar, MB]*
   (-(MetricTensor[li2, li7]*MetricTensor[li3, li5]*
      (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 2], Index[Gluon, 8], 
        Index[Gluon, 7], Index[Gluon, 9]])) + MetricTensor[li2, li3]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 2], Index[Gluon, 8], 
       Index[Gluon, 7], Index[Gluon, 9]] - SUNF[Index[Gluon, 2], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 7]]) + 
    MetricTensor[li2, li5]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 2], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 7]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    p2 - q - qb - qc - qcbar, MB]*PropagatorDenominator[-p2 + q + qc + qcbar, 
    0]*(-(MetricTensor[li2, li7]*MetricTensor[li3, li5]*
      (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 2], Index[Gluon, 8], 
        Index[Gluon, 7], Index[Gluon, 9]])) + MetricTensor[li2, li3]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 2], Index[Gluon, 8], 
       Index[Gluon, 7], Index[Gluon, 9]] - SUNF[Index[Gluon, 2], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 7]]) + 
    MetricTensor[li2, li5]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 2], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 7]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qb - qbbar - 
       qcbar]) . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -p2 + q + qb + qbbar, 0]*PropagatorDenominator[
    -p2 + q + qb + qbbar + qcbar, MC]*
   (-(MetricTensor[li2, li7]*MetricTensor[li3, li5]*
      (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 2], Index[Gluon, 8], 
        Index[Gluon, 7], Index[Gluon, 9]])) + MetricTensor[li2, li3]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 2], Index[Gluon, 8], 
       Index[Gluon, 7], Index[Gluon, 9]] - SUNF[Index[Gluon, 2], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 7]]) + 
    MetricTensor[li2, li5]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 2], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 7]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*
     SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -p2 + q + qb + qbbar, 0]*PropagatorDenominator[p2 - q - qb - qbbar - qc, 
    MC]*(-(MetricTensor[li2, li7]*MetricTensor[li3, li5]*
      (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 2], Index[Gluon, 8], 
        Index[Gluon, 7], Index[Gluon, 9]])) + MetricTensor[li2, li3]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 2], Index[Gluon, 8], 
       Index[Gluon, 7], Index[Gluon, 9]] - SUNF[Index[Gluon, 2], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 7]]) + 
    MetricTensor[li2, li5]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 2], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 7]]))), 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qcbar, MC]*(FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]]*(-(MetricTensor[li2, li9]*MetricTensor[li5, li7]*
     (SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9], 
       Index[Gluon, 10]] + SUNF[Index[Gluon, 2], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 10]])) + MetricTensor[li2, li5]*
    MetricTensor[li7, li9]*(SUNF[Index[Gluon, 2], Index[Gluon, 9], 
      Index[Gluon, 8], Index[Gluon, 10]] - SUNF[Index[Gluon, 2], 
      Index[Gluon, 10], Index[Gluon, 9], Index[Gluon, 8]]) + 
   MetricTensor[li2, li7]*MetricTensor[li5, li9]*
    (SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9], 
      Index[Gluon, 10]] + SUNF[Index[Gluon, 2], Index[Gluon, 10], 
      Index[Gluon, 9], Index[Gluon, 8]])), 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qb + qbbar, 0]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]]*(-(MetricTensor[li2, li9]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 9], 
       Index[Gluon, 10]] + SUNF[Index[Gluon, 2], Index[Gluon, 9], 
       Index[Gluon, 7], Index[Gluon, 10]])) + MetricTensor[li2, li3]*
    MetricTensor[li7, li9]*(SUNF[Index[Gluon, 2], Index[Gluon, 9], 
      Index[Gluon, 7], Index[Gluon, 10]] - SUNF[Index[Gluon, 2], 
      Index[Gluon, 10], Index[Gluon, 9], Index[Gluon, 7]]) + 
   MetricTensor[li2, li7]*MetricTensor[li3, li9]*
    (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 9], 
      Index[Gluon, 10]] + SUNF[Index[Gluon, 2], Index[Gluon, 10], 
      Index[Gluon, 9], Index[Gluon, 7]])), 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 - qc - qcbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + q + qb + qbbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 - q - qb - qbbar + qc + qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
  PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]]*(-(MetricTensor[li2, li9]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 9], 
       Index[Gluon, 10]] + SUNF[Index[Gluon, 2], Index[Gluon, 9], 
       Index[Gluon, 7], Index[Gluon, 10]])) + MetricTensor[li2, li3]*
    MetricTensor[li7, li9]*(SUNF[Index[Gluon, 2], Index[Gluon, 9], 
      Index[Gluon, 7], Index[Gluon, 10]] - SUNF[Index[Gluon, 2], 
      Index[Gluon, 10], Index[Gluon, 9], Index[Gluon, 7]]) + 
   MetricTensor[li2, li7]*MetricTensor[li3, li9]*
    (SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 9], 
      Index[Gluon, 10]] + SUNF[Index[Gluon, 2], Index[Gluon, 10], 
      Index[Gluon, 9], Index[Gluon, 7]])), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
    0]*PropagatorDenominator[-qc - qcbar, 0]*
   (-(MetricTensor[li1, li7]*MetricTensor[li3, li5]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 8], 
        Index[Gluon, 7], Index[Gluon, 9]])) + MetricTensor[li1, li3]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 1], Index[Gluon, 8], 
       Index[Gluon, 7], Index[Gluon, 9]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 7]]) + 
    MetricTensor[li1, li5]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 7]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
    -p2 + qbbar, MB]*PropagatorDenominator[-qc - qcbar, 0]*
   (-(MetricTensor[li1, li7]*MetricTensor[li3, li5]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 8], 
        Index[Gluon, 7], Index[Gluon, 9]])) + MetricTensor[li1, li3]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 1], Index[Gluon, 8], 
       Index[Gluon, 7], Index[Gluon, 9]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 7]]) + 
    MetricTensor[li1, li5]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 7]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[p2 - qc - qcbar, 0]*
   (-(MetricTensor[li1, li7]*MetricTensor[li3, li5]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 8], 
        Index[Gluon, 7], Index[Gluon, 9]])) + MetricTensor[li1, li3]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 1], Index[Gluon, 8], 
       Index[Gluon, 7], Index[Gluon, 9]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 7]]) + 
    MetricTensor[li1, li5]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 7]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*
     SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    p2 - qc - qcbar, 0]*PropagatorDenominator[-p2 + qcbar, MC]*
   (-(MetricTensor[li1, li7]*MetricTensor[li3, li5]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 8], 
        Index[Gluon, 7], Index[Gluon, 9]])) + MetricTensor[li1, li3]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 1], Index[Gluon, 8], 
       Index[Gluon, 7], Index[Gluon, 9]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 7]]) + 
    MetricTensor[li1, li5]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 7]]))), 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qcbar, MC]*(FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-qc - qcbar, 0]*
  SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]]*
  (-(MetricTensor[li1, li9]*MetricTensor[li5, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9], 
       Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 10]])) + MetricTensor[li1, li5]*
    MetricTensor[li7, li9]*(SUNF[Index[Gluon, 1], Index[Gluon, 9], 
      Index[Gluon, 8], Index[Gluon, 10]] - SUNF[Index[Gluon, 1], 
      Index[Gluon, 10], Index[Gluon, 9], Index[Gluon, 8]]) + 
   MetricTensor[li1, li7]*MetricTensor[li5, li9]*
    (SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9], 
      Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 10], 
      Index[Gluon, 9], Index[Gluon, 8]])), 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p2 - qb - qbbar, li6]*MetricTensor[li2, li4] + 
   FourVector[2*p2 - qb - qbbar, li4]*MetricTensor[li2, li6] + 
   FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[-qc - qcbar, 0]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]*
  (-(MetricTensor[li1, li9]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 9], 
       Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 7], Index[Gluon, 10]])) + MetricTensor[li1, li3]*
    MetricTensor[li7, li9]*(SUNF[Index[Gluon, 1], Index[Gluon, 9], 
      Index[Gluon, 7], Index[Gluon, 10]] - SUNF[Index[Gluon, 1], 
      Index[Gluon, 10], Index[Gluon, 9], Index[Gluon, 7]]) + 
   MetricTensor[li1, li7]*MetricTensor[li3, li9]*
    (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 9], 
      Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 10], 
      Index[Gluon, 9], Index[Gluon, 7]])), 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p2 - qc - qcbar, li6]*MetricTensor[li2, li4] + 
   FourVector[2*p2 - qc - qcbar, li4]*MetricTensor[li2, li6] + 
   FourVector[-p2 + 2*(qc + qcbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[qc + qcbar, 
   0]*SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]*
  (-(MetricTensor[li1, li9]*MetricTensor[li3, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 9], 
       Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 7], Index[Gluon, 10]])) + MetricTensor[li1, li3]*
    MetricTensor[li7, li9]*(SUNF[Index[Gluon, 1], Index[Gluon, 9], 
      Index[Gluon, 7], Index[Gluon, 10]] - SUNF[Index[Gluon, 1], 
      Index[Gluon, 10], Index[Gluon, 9], Index[Gluon, 7]]) + 
   MetricTensor[li1, li7]*MetricTensor[li3, li9]*
    (SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 9], 
      Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 10], 
      Index[Gluon, 9], Index[Gluon, 7]])), 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[q - qb - qbbar, li8]*MetricTensor[li3, li5] + 
   FourVector[-2*q - qb - qbbar, li5]*MetricTensor[li3, li8] + 
   FourVector[q + 2*(qb + qbbar), li3]*MetricTensor[li5, li8])*
  MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[
   -qc - qcbar, 0]*SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 10]]*
  (-(MetricTensor[li1, li9]*MetricTensor[li2, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 9], 
       Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 2], Index[Gluon, 10]])) + MetricTensor[li1, li2]*
    MetricTensor[li7, li9]*(SUNF[Index[Gluon, 1], Index[Gluon, 9], 
      Index[Gluon, 2], Index[Gluon, 10]] - SUNF[Index[Gluon, 1], 
      Index[Gluon, 10], Index[Gluon, 9], Index[Gluon, 2]]) + 
   MetricTensor[li1, li7]*MetricTensor[li2, li9]*
    (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 9], 
      Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 10], 
      Index[Gluon, 9], Index[Gluon, 2]])), 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
  (FourVector[q - qc - qcbar, li8]*MetricTensor[li3, li7] + 
   FourVector[-2*q - qc - qcbar, li7]*MetricTensor[li3, li8] + 
   FourVector[q + 2*(qc + qcbar), li3]*MetricTensor[li7, li8])*
  MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   -q - qc - qcbar, 0]*SUNF[Index[Gluon, 7], Index[Gluon, 9], 
   Index[Gluon, 10]]*(-(MetricTensor[li1, li9]*MetricTensor[li2, li5]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8], 
       Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 8], 
       Index[Gluon, 2], Index[Gluon, 10]])) + MetricTensor[li1, li2]*
    MetricTensor[li5, li9]*(SUNF[Index[Gluon, 1], Index[Gluon, 8], 
      Index[Gluon, 2], Index[Gluon, 10]] - SUNF[Index[Gluon, 1], 
      Index[Gluon, 10], Index[Gluon, 8], Index[Gluon, 2]]) + 
   MetricTensor[li1, li5]*MetricTensor[li2, li9]*
    (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8], 
      Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 10], 
      Index[Gluon, 8], Index[Gluon, 2]])), 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
  (FourVector[qb + qbbar - qc - qcbar, li8]*MetricTensor[li5, li7] + 
   FourVector[-2*qb - 2*qbbar - qc - qcbar, li7]*MetricTensor[li5, li8] + 
   FourVector[qb + qbbar + 2*(qc + qcbar), li5]*MetricTensor[li7, li8])*
  MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   -qb - qbbar - qc - qcbar, 0]*SUNF[Index[Gluon, 8], Index[Gluon, 9], 
   Index[Gluon, 10]]*(-(MetricTensor[li1, li9]*MetricTensor[li2, li3]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
       Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 7], 
       Index[Gluon, 2], Index[Gluon, 10]])) + MetricTensor[li1, li2]*
    MetricTensor[li3, li9]*(SUNF[Index[Gluon, 1], Index[Gluon, 7], 
      Index[Gluon, 2], Index[Gluon, 10]] - SUNF[Index[Gluon, 1], 
      Index[Gluon, 10], Index[Gluon, 7], Index[Gluon, 2]]) + 
   MetricTensor[li1, li3]*MetricTensor[li2, li9]*
    (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
      Index[Gluon, 10]] + SUNF[Index[Gluon, 1], Index[Gluon, 10], 
      Index[Gluon, 7], Index[Gluon, 2]])), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[-q - qc - qcbar, 0]*
   (-(MetricTensor[li1, li7]*MetricTensor[li2, li5]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 8], 
        Index[Gluon, 2], Index[Gluon, 9]])) + MetricTensor[li1, li2]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 1], Index[Gluon, 8], 
       Index[Gluon, 2], Index[Gluon, 9]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 2]]) + 
    MetricTensor[li1, li5]*MetricTensor[li2, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 2]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*
     SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -q - qc - qcbar, 0]*PropagatorDenominator[q + qcbar, MC]*
   (-(MetricTensor[li1, li7]*MetricTensor[li2, li5]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 8], 
        Index[Gluon, 2], Index[Gluon, 9]])) + MetricTensor[li1, li2]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 1], Index[Gluon, 8], 
       Index[Gluon, 2], Index[Gluon, 9]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 2]]) + 
    MetricTensor[li1, li5]*MetricTensor[li2, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 2]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -qb - qbbar - qc, MC]*PropagatorDenominator[-qb - qbbar - qc - qcbar, 0]*
   (-(MetricTensor[li1, li7]*MetricTensor[li2, li3]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 7], 
        Index[Gluon, 2], Index[Gluon, 9]])) + MetricTensor[li1, li2]*
     MetricTensor[li3, li7]*(SUNF[Index[Gluon, 1], Index[Gluon, 7], 
       Index[Gluon, 2], Index[Gluon, 9]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 9], Index[Gluon, 7], Index[Gluon, 2]]) + 
    MetricTensor[li1, li3]*MetricTensor[li2, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 7], Index[Gluon, 2]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*
     SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -qb - qbbar - qc - qcbar, 0]*PropagatorDenominator[qb + qbbar + qcbar, 
    MC]*(-(MetricTensor[li1, li7]*MetricTensor[li2, li3]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 7], 
        Index[Gluon, 2], Index[Gluon, 9]])) + MetricTensor[li1, li2]*
     MetricTensor[li3, li7]*(SUNF[Index[Gluon, 1], Index[Gluon, 7], 
       Index[Gluon, 2], Index[Gluon, 9]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 9], Index[Gluon, 7], Index[Gluon, 2]]) + 
    MetricTensor[li1, li3]*MetricTensor[li2, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 7], Index[Gluon, 2]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-q - qb - qbbar, 
    0]*PropagatorDenominator[-qc - qcbar, 0]*
   (-(MetricTensor[li1, li7]*MetricTensor[li2, li5]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 8], 
        Index[Gluon, 2], Index[Gluon, 9]])) + MetricTensor[li1, li2]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 1], Index[Gluon, 8], 
       Index[Gluon, 2], Index[Gluon, 9]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 2]]) + 
    MetricTensor[li1, li5]*MetricTensor[li2, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 2]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[q + qbbar, 
    MB]*PropagatorDenominator[-qc - qcbar, 0]*
   (-(MetricTensor[li1, li7]*MetricTensor[li2, li5]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 8], 
        Index[Gluon, 2], Index[Gluon, 9]])) + MetricTensor[li1, li2]*
     MetricTensor[li5, li7]*(SUNF[Index[Gluon, 1], Index[Gluon, 8], 
       Index[Gluon, 2], Index[Gluon, 9]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 9], Index[Gluon, 8], Index[Gluon, 2]]) + 
    MetricTensor[li1, li5]*MetricTensor[li2, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 8], Index[Gluon, 2]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qc - qcbar, MB]*PropagatorDenominator[
    -qb - qbbar - qc - qcbar, 0]*PropagatorDenominator[qc + qcbar, 0]*
   (-(MetricTensor[li1, li7]*MetricTensor[li2, li3]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
        Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 7], 
        Index[Gluon, 2], Index[Gluon, 9]])) + MetricTensor[li1, li2]*
     MetricTensor[li3, li7]*(SUNF[Index[Gluon, 1], Index[Gluon, 7], 
       Index[Gluon, 2], Index[Gluon, 9]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 9], Index[Gluon, 7], Index[Gluon, 2]]) + 
    MetricTensor[li1, li3]*MetricTensor[li2, li7]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
       Index[Gluon, 9]] + SUNF[Index[Gluon, 1], Index[Gluon, 9], 
       Index[Gluon, 7], Index[Gluon, 2]]))), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar - qc - qcbar, 0]*
   PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    qbbar + qc + qcbar, MB]*(-(MetricTensor[li1, li5]*MetricTensor[li2, li3]*
      (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
        Index[Gluon, 8]] + SUNF[Index[Gluon, 1], Index[Gluon, 7], 
        Index[Gluon, 2], Index[Gluon, 8]])) + MetricTensor[li1, li2]*
     MetricTensor[li3, li5]*(SUNF[Index[Gluon, 1], Index[Gluon, 7], 
       Index[Gluon, 2], Index[Gluon, 8]] - SUNF[Index[Gluon, 1], 
       Index[Gluon, 8], Index[Gluon, 7], Index[Gluon, 2]]) + 
    MetricTensor[li1, li3]*MetricTensor[li2, li5]*
     (SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 7], 
       Index[Gluon, 8]] + SUNF[Index[Gluon, 1], Index[Gluon, 8], 
       Index[Gluon, 7], Index[Gluon, 2]]))), 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li10, li11]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  (FourVector[-q + qb + qbbar + qc + qcbar, li5]*MetricTensor[li10, li3] + 
   FourVector[-q - 2*(qb + qbbar + qc + qcbar), li3]*
    MetricTensor[li10, li5] + FourVector[2*q + qb + qbbar + qc + qcbar, li10]*
    MetricTensor[li3, li5])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
  (FourVector[-2*qb - 2*qbbar - qc - qcbar, li9]*MetricTensor[li11, li7] + 
   FourVector[qb + qbbar + 2*(qc + qcbar), li7]*MetricTensor[li11, li9] + 
   FourVector[qb + qbbar - qc - qcbar, li11]*MetricTensor[li7, li9])*
  MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   qb + qbbar + qc + qcbar, 0]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]]*SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 11]]*
  SUNF[Index[Gluon, 9], Index[Gluon, 10], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li10, li11]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  (FourVector[-2*q - qb - qbbar, li7]*MetricTensor[li10, li3] + 
   FourVector[q + 2*(qb + qbbar), li3]*MetricTensor[li10, li7] + 
   FourVector[q - qb - qbbar, li10]*MetricTensor[li3, li7])*
  MetricTensor[li4, li5]*(FourVector[2*q + 2*qb + 2*qbbar + qc + qcbar, li9]*
    MetricTensor[li11, li5] + FourVector[-q - qb - qbbar + qc + qcbar, li5]*
    MetricTensor[li11, li9] + FourVector[-q - qb - qbbar - 2*qc - 2*qcbar, 
     li11]*MetricTensor[li5, li9])*MetricTensor[li6, li7]*
  MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[
   -qc - qcbar, 0]*PropagatorDenominator[q + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]]*
  SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 11]]*
  SUNF[Index[Gluon, 8], Index[Gluon, 10], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li10, li11]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  (FourVector[-2*q - qc - qcbar, li9]*MetricTensor[li10, li3] + 
   FourVector[q + 2*(qc + qcbar), li3]*MetricTensor[li10, li9] + 
   FourVector[q - qc - qcbar, li10]*MetricTensor[li3, li9])*
  MetricTensor[li4, li5]*(FourVector[2*q + qb + qbbar + 2*qc + 2*qcbar, li7]*
    MetricTensor[li11, li5] + FourVector[-q + qb + qbbar - qc - qcbar, li5]*
    MetricTensor[li11, li7] + FourVector[-q - 2*qb - 2*qbbar - qc - qcbar, 
     li11]*MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   -q - qc - qcbar, 0]*PropagatorDenominator[q + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]]*
  SUNF[Index[Gluon, 7], Index[Gluon, 10], Index[Gluon, 11]]*
  SUNF[Index[Gluon, 8], Index[Gluon, 9], Index[Gluon, 11]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[
   -q - qb - qbbar - qc, MC]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[
   qb + qbbar + qcbar, MC]*PropagatorDenominator[q + qb + qbbar + qc + qcbar, 
   0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-q - 2*qb - 2*qbbar - qc - qcbar, li9]*
     MetricTensor[li5, li7] + FourVector[2*q + qb + qbbar + 2*qc + 2*qcbar, 
      li7]*MetricTensor[li5, li9] + FourVector[-q + qb + qbbar - qc - qcbar, 
      li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[-q - qc - qcbar, 0]*PropagatorDenominator[
    q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 8], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[q + qcbar, MC]*PropagatorDenominator[
   q + qb + qbbar + qcbar, MC]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qb - qbbar - qc, MC]*PropagatorDenominator[
   q + qcbar, MC]*PropagatorDenominator[q + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-q - 2*qb - 2*qbbar - qc - qcbar, li9]*
     MetricTensor[li5, li7] + FourVector[2*q + qb + qbbar + 2*qc + 2*qcbar, 
      li7]*MetricTensor[li5, li9] + FourVector[-q + qb + qbbar - qc - qcbar, 
      li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -q - qc - qcbar, 0]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[q + qb + qbbar + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]]*
   SUNF[Index[Gluon, 8], Index[Gluon, 9], Index[Gluon, 10]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-q - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[qb + qbbar + qcbar, MC]*
  PropagatorDenominator[q + qb + qbbar + qcbar, MC]*
  PropagatorDenominator[q + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[q - qb - qbbar, li9]*MetricTensor[li3, li7] + 
    FourVector[-2*q - qb - qbbar, li7]*MetricTensor[li3, li9] + 
    FourVector[q + 2*(qb + qbbar), li3]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
   PropagatorDenominator[q + qb + qbbar, 0]*PropagatorDenominator[
    q + qb + qbbar + qcbar, MC]*PropagatorDenominator[
    q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 7], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[q + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qb - qbbar - qc, MC]*PropagatorDenominator[
   -q - qb - qbbar - qc, MC]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[q - qb - qbbar, li9]*MetricTensor[li3, li7] + 
    FourVector[-2*q - qb - qbbar, li7]*MetricTensor[li3, li9] + 
    FourVector[q + 2*(qb + qbbar), li3]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
   PropagatorDenominator[q + qb + qbbar, 0]*PropagatorDenominator[
    -q - qb - qbbar - qc, MC]*PropagatorDenominator[
    q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 7], Index[Gluon, 9], 
    Index[Gluon, 10]]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*
     SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*
   (FourVector[2*q + qb + qbbar + qc + qcbar, li9]*MetricTensor[li3, li5] + 
    FourVector[-q + qb + qbbar + qc + qcbar, li5]*MetricTensor[li3, li9] + 
    FourVector[-q - 2*(qb + qbbar + qc + qcbar), li3]*MetricTensor[li5, li9])*
   MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
   PropagatorDenominator[-qb - qbbar - qc, MC]*PropagatorDenominator[
    -qb - qbbar - qc - qcbar, 0]*PropagatorDenominator[
    q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 7], Index[Gluon, 8], 
    Index[Gluon, 10]]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*
   (FourVector[2*q + qb + qbbar + qc + qcbar, li9]*MetricTensor[li3, li5] + 
    FourVector[-q + qb + qbbar + qc + qcbar, li5]*MetricTensor[li3, li9] + 
    FourVector[-q - 2*(qb + qbbar + qc + qcbar), li3]*MetricTensor[li5, li9])*
   MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
   PropagatorDenominator[-qb - qbbar - qc - qcbar, 0]*
   PropagatorDenominator[qb + qbbar + qcbar, MC]*
   PropagatorDenominator[q + qb + qbbar + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 10]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qb, MB]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   -q - qb - qc - qcbar, MB]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-2*q - 2*qb - 2*qbbar - qc - qcbar, li9]*
     MetricTensor[li5, li7] + FourVector[q + qb + qbbar + 2*qc + 2*qcbar, 
      li7]*MetricTensor[li5, li9] + FourVector[q + qb + qbbar - qc - qcbar, 
      li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-q - qb - qbbar, 
    0]*PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 8], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qb, MB]*
  PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
   qbbar + qc + qcbar, MB]*PropagatorDenominator[q + qb + qbbar + qc + qcbar, 
   0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-q - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[-q - qbbar]) . 
   ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[q + qbbar, MB]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   q + qbbar + qc + qcbar, MB]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*
     SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-2*q - 2*qb - 2*qbbar - qc - qcbar, li9]*
     MetricTensor[li5, li7] + FourVector[q + qb + qbbar + 2*qc + 2*qcbar, 
      li7]*MetricTensor[li5, li9] + FourVector[q + qb + qbbar - qc - qcbar, 
      li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[q + qbbar, 
    MB]*PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 8], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[q + qbbar, MB]*
  PropagatorDenominator[-qb - qc - qcbar, MB]*PropagatorDenominator[
   qc + qcbar, 0]*PropagatorDenominator[q + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[2*q + qc + qcbar, li9]*MetricTensor[li3, li7] + 
    FourVector[-q + qc + qcbar, li7]*MetricTensor[li3, li9] + 
    FourVector[-q - 2*(qc + qcbar), li3]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qc - qcbar, 0]*
   PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
    q + qbbar + qc + qcbar, MB]*PropagatorDenominator[
    q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 7], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[-qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qc + qcbar, 0]*
  PropagatorDenominator[qbbar + qc + qcbar, MB]*
  PropagatorDenominator[q + qbbar + qc + qcbar, MB]*
  PropagatorDenominator[q + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*
     SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[2*q + qc + qcbar, li9]*MetricTensor[li3, li7] + 
    FourVector[-q + qc + qcbar, li7]*MetricTensor[li3, li9] + 
    FourVector[-q - 2*(qc + qcbar), li3]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qc - qcbar, 0]*
   PropagatorDenominator[-q - qb - qc - qcbar, MB]*
   PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
    q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 7], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qc - qcbar, MB]*
  PropagatorDenominator[-q - qb - qc - qcbar, MB]*
  PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*
     SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*
   (FourVector[2*q + qb + qbbar + qc + qcbar, li9]*MetricTensor[li3, li5] + 
    FourVector[-q + qb + qbbar + qc + qcbar, li5]*MetricTensor[li3, li9] + 
    FourVector[-q - 2*(qb + qbbar + qc + qcbar), li3]*MetricTensor[li5, li9])*
   MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qc - qcbar, MB]*
   PropagatorDenominator[-qb - qbbar - qc - qcbar, 0]*
   PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 7], Index[Gluon, 8], 
    Index[Gluon, 10]]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*
     SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[-qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 + p2, li4]*
     MetricTensor[li1, li2] + FourVector[p1 + q + qb + qbbar + qc + qcbar, 
      li2]*MetricTensor[li1, li4] + 
    FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li2, li4])*MetricTensor[li4, li5]*
   (FourVector[2*q + qb + qbbar + qc + qcbar, li7]*MetricTensor[li3, li5] + 
    FourVector[-q + qb + qbbar + qc + qcbar, li5]*MetricTensor[li3, li7] + 
    FourVector[-q - 2*(qb + qbbar + qc + qcbar), li3]*MetricTensor[li5, li7])*
   MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[
    -qb - qbbar - qc - qcbar, 0]*PropagatorDenominator[qc + qcbar, 0]*
   PropagatorDenominator[qbbar + qc + qcbar, MB]*
   PropagatorDenominator[q + qb + qbbar + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-q - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qb, MB]*
  PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[
   q + qb + qbbar + qcbar, MC]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[q + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qb, MB]*
  PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[
   -q - qb - qbbar - qc, MC]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-q - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qb - qbbar, 0]*
  PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[
   q + qb + qbbar + qcbar, MC]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[q + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qb - qbbar, 0]*
  PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[
   -q - qb - qbbar - qc, MC]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-q - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qc, MC]*
  PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
   q + qbbar + qc + qcbar, MB]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qc, MC]*
  PropagatorDenominator[-q - qb - qc - qcbar, MB]*
  PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-q - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[q + qcbar, MC]*
  PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
   q + qbbar + qc + qcbar, MB]*PropagatorDenominator[
   q + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 2], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 + p2, li4]*MetricTensor[li1, li2] + 
   FourVector[p1 + q + qb + qbbar + qc + qcbar, li2]*MetricTensor[li1, li4] + 
   FourVector[-p2 - q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li2, li4])*
  MetricTensor[li4, li5]*MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qb - qc - qcbar, MB]*
  PropagatorDenominator[q + qcbar, MC]*PropagatorDenominator[q + qc + qcbar, 
   0]*PropagatorDenominator[q + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 2], Index[Gluon, 8]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar - qc - 
       qcbar]) . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[-qc - qcbar, 
    0]*PropagatorDenominator[-p2 + qbbar + qc + qcbar, MB]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar - qc - 
       qcbar]) . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
    -p2 + q + qbbar, MB]*PropagatorDenominator[-qc - qcbar, 0]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
  (FourVector[q - qc - qcbar, li6]*MetricTensor[li3, li5] + 
   FourVector[-2*q - qc - qcbar, li5]*MetricTensor[li3, li6] + 
   FourVector[q + 2*(qc + qcbar), li3]*MetricTensor[li5, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-p2 + qbbar, MB]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   -q - qc - qcbar, 0]*PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, 
   MB]*SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[-q - qc - qcbar, 0]*PropagatorDenominator[
    -p2 + q + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
    -q - qc - qcbar, 0]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[
    -p2 + q + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p2 + 2*q + qc + qcbar, li7]*MetricTensor[li3, li5] + 
   FourVector[-p2 - q + qc + qcbar, li5]*MetricTensor[li3, li7] + 
   FourVector[2*p2 - q - 2*(qc + qcbar), li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qc, MC]*
  PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*PropagatorDenominator[
   -p2 + q + qbbar + qc + qcbar, MB]*SUNF[Index[Gluon, 7], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[p2 - q - qc, MC]*
   PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[-p2 + qc + qcbar, 
    0]*PropagatorDenominator[-p2 + qbbar + qc + qcbar, MB]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[
    p2 - qc - qcbar, 0]*PropagatorDenominator[-p2 + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[-p2 + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p2 + 2*q + qc + qcbar, li7]*MetricTensor[li3, li5] + 
   FourVector[-p2 - q + qc + qcbar, li5]*MetricTensor[li3, li7] + 
   FourVector[2*p2 - q - 2*(qc + qcbar), li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qc - qcbar, 0]*
  PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*PropagatorDenominator[
   -p2 + q + qbbar + qc + qcbar, MB]*SUNF[Index[Gluon, 7], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[p2 - q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
    -p2 + q + qcbar, MC]*PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
    -p2 + qc + qcbar, 0]*PropagatorDenominator[-p2 + qbbar + qc + qcbar, MB]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar - qc - 
       qcbar]) . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p2 - q, li4]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li4] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-2*p2 + 2*q + qc + qcbar, li9]*MetricTensor[li5, li7] + 
    FourVector[p2 - q + qc + qcbar, li7]*MetricTensor[li5, li9] + 
    FourVector[p2 - q - 2*(qc + qcbar), li5]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    -p2 + q + qc + qcbar, 0]*PropagatorDenominator[
    -p2 + q + qbbar + qc + qcbar, MB]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 8], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[p2 - q - qbbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[-p2 + q + qbbar, MB]*PropagatorDenominator[
   -qc - qcbar, 0]*PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]*
  SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
   qbbar + qc + qcbar, MB]*PropagatorDenominator[
   -p2 + q + qbbar + qc + qcbar, MB]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar - qc - 
       qcbar]) . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-q - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[-qc - qcbar, 0]*
   PropagatorDenominator[q + qbbar + qc + qcbar, MB]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar - qc - 
       qcbar]) . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - q - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[
    -p2 + q + qbbar, MB]*PropagatorDenominator[-qc - qcbar, 0]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
  (FourVector[-p2 - qc - qcbar, li6]*MetricTensor[li2, li4] + 
   FourVector[2*p2 - qc - qcbar, li4]*MetricTensor[li2, li6] + 
   FourVector[-p2 + 2*(qc + qcbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[q + qbbar, MB]*
  PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[qc + qcbar, 
   0]*PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-q - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
  (FourVector[2*q + qc + qcbar, li7]*MetricTensor[li3, li5] + 
   FourVector[-q + qc + qcbar, li5]*MetricTensor[li3, li7] + 
   FourVector[-q - 2*(qc + qcbar), li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qc - qcbar, 0]*
  PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
   q + qbbar + qc + qcbar, MB]*PropagatorDenominator[
   -p2 + q + qbbar + qc + qcbar, MB]*SUNF[Index[Gluon, 7], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar - qc - 
       qcbar]) . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-q - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    qbbar + qc + qcbar, MB]*PropagatorDenominator[q + qbbar + qc + qcbar, MB]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar - qc - 
       qcbar]) . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-2*p2 + q + qc + qcbar, li6]*MetricTensor[li2, li4] + 
    FourVector[p2 + q + qc + qcbar, li4]*MetricTensor[li2, li6] + 
    FourVector[p2 - 2*(q + qc + qcbar), li2]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[2*q + qc + qcbar, li9]*
     MetricTensor[li3, li7] + FourVector[-q + qc + qcbar, li7]*
     MetricTensor[li3, li9] + FourVector[-q - 2*(qc + qcbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    p2 - q - qc - qcbar, 0]*PropagatorDenominator[q + qc + qcbar, 0]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]*
   SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar - qc - 
       qcbar]) . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-p2 - qc - qcbar, li6]*MetricTensor[li2, li4] + 
    FourVector[2*p2 - qc - qcbar, li4]*MetricTensor[li2, li6] + 
    FourVector[-p2 + 2*(qc + qcbar), li2]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[p2 + q - qc - qcbar, li9]*
     MetricTensor[li3, li7] + FourVector[p2 - 2*q - qc - qcbar, li7]*
     MetricTensor[li3, li9] + FourVector[-2*p2 + q + 2*(qc + qcbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[
    qc + qcbar, 0]*PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]*
   SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
  (FourVector[-2*p2 + qc + qcbar, li6]*MetricTensor[li2, li4] + 
   FourVector[p2 + qc + qcbar, li4]*MetricTensor[li2, li6] + 
   FourVector[p2 - 2*(qc + qcbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qc - qcbar, 0]*
  PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
   -p2 + qbbar + qc + qcbar, MB]*PropagatorDenominator[
   -p2 + q + qbbar + qc + qcbar, MB]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar - qc - 
       qcbar]) . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    qbbar + qc + qcbar, MB]*PropagatorDenominator[-p2 + qbbar + qc + qcbar, 
    MB]*PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[p2 - q - qc, MC]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*PropagatorDenominator[
   -p2 + q + qbbar + qc + qcbar, MB]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - q - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[-p2 + q + qcbar, MC]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*PropagatorDenominator[
   -p2 + q + qbbar + qc + qcbar, MB]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-q - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[q + qc + qcbar, 
    0]*PropagatorDenominator[q + qbbar + qc + qcbar, MB]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-2*p2 + q + qc + qcbar, li6]*MetricTensor[li2, li4] + 
   FourVector[p2 + q + qc + qcbar, li4]*MetricTensor[li2, li6] + 
   FourVector[p2 - 2*(q + qc + qcbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qc, MC]*
  PropagatorDenominator[p2 - q - qc - qcbar, 0]*
  PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
   -p2 + q + qbbar + qc + qcbar, MB]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[p2 - q - qc, MC]*
   PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-q - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qcbar, MC]*PropagatorDenominator[q + qc + qcbar, 
    0]*PropagatorDenominator[q + qbbar + qc + qcbar, MB]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-2*p2 + q + qc + qcbar, li6]*MetricTensor[li2, li4] + 
   FourVector[p2 + q + qc + qcbar, li4]*MetricTensor[li2, li6] + 
   FourVector[p2 - 2*(q + qc + qcbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q - qc - qcbar, 0]*
  PropagatorDenominator[q + qcbar, MC]*PropagatorDenominator[q + qc + qcbar, 
   0]*PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[p2 - q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qcbar, MC]*PropagatorDenominator[
    -p2 + q + qcbar, MC]*PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
   PropagatorDenominator[-p2 + q + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[-qc - qcbar, 0]*
   PropagatorDenominator[p2 - qb - qc - qcbar, MB]*
   PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[-p2 + q + qb]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - q - qb, MB]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    p2 - q - qb - qc - qcbar, MB]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li4, li5]*(FourVector[q - qc - qcbar, li6]*
    MetricTensor[li3, li5] + FourVector[-2*q - qc - qcbar, li5]*
    MetricTensor[li3, li6] + FourVector[q + 2*(qc + qcbar), li3]*
    MetricTensor[li5, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[-qc - qcbar, 0]*
  PropagatorDenominator[-q - qc - qcbar, 0]*PropagatorDenominator[
   p2 - q - qb - qc - qcbar, MB]*SUNF[Index[Gluon, 7], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[-q - qc - qcbar, 0]*PropagatorDenominator[
    p2 - q - qb - qc - qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[-q - qc - qcbar, 
    0]*PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
   PropagatorDenominator[q + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[q + qbbar, MB]*
   PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
    0]*PropagatorDenominator[q + qcbar, MC]*PropagatorDenominator[
    -p2 + q + qb + qbbar + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[p2 + q - qb - qbbar, li7]*
    MetricTensor[li3, li5] + FourVector[p2 - 2*q - qb - qbbar, li5]*
    MetricTensor[li3, li7] + FourVector[-2*p2 + q + 2*(qb + qbbar), li3]*
    MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
   0]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
  PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]*
  SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[-p2 + q + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - q - qb, MB]*
   PropagatorDenominator[p2 - q - qb - qbbar, 0]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
    0]*PropagatorDenominator[-p2 + qb + qbbar + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[q + qbbar, MB]*
   PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
    0]*PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[
    p2 - q - qb - qbbar - qc, MC]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[p2 + q - qb - qbbar, li7]*
    MetricTensor[li3, li5] + FourVector[p2 - 2*q - qb - qbbar, li5]*
    MetricTensor[li3, li7] + FourVector[-2*p2 + q + 2*(qb + qbbar), li3]*
    MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
   0]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
  PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]*
  SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[-p2 + q + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - q - qb, MB]*
   PropagatorDenominator[p2 - q - qb - qbbar, 0]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
    0]*PropagatorDenominator[p2 - qb - qbbar - qc, MC]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[-p2 + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb, MB]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   p2 - qb - qc - qcbar, MB]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[2*p2 - 2*qb - 2*qbbar - qc - qcbar, li9]*
     MetricTensor[li5, li7] + FourVector[-p2 + qb + qbbar + 2*qc + 2*qcbar, 
      li7]*MetricTensor[li5, li9] + FourVector[-p2 + qb + qbbar - qc - qcbar, 
      li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
    0]*PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 8], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb, MB]*
  PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
   qbbar + qc + qcbar, MB]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[-q - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[q + qbbar, MB]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    q + qbbar + qc + qcbar, MB]), -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[q + qbbar, MB]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    p2 - qb - qc - qcbar, MB]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
  (FourVector[-p1 - qc - qcbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + q + qb + qbbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 - q - qb - qbbar + qc + qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb, MB]*
  PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[
   -p2 + q + qb + qbbar, 0]*PropagatorDenominator[qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[-q - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li4, li5]*(FourVector[2*q + qc + qcbar, li7]*
    MetricTensor[li3, li5] + FourVector[-q + qc + qcbar, li5]*
    MetricTensor[li3, li7] + FourVector[-q - 2*(qc + qcbar), li3]*
    MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[-qc - qcbar, 0]*
  PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
   q + qbbar + qc + qcbar, MB]*SUNF[Index[Gluon, 7], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[-q - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[qc + qcbar, 0]*
   PropagatorDenominator[qbbar + qc + qcbar, MB]*
   PropagatorDenominator[q + qbbar + qc + qcbar, MB]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-p2 + q + qb]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li4, li5]*(FourVector[-p1 + p2 - q - qb - qbbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 + qc + qcbar, li4]*
    MetricTensor[li1, li6] + FourVector[-p2 + q + qb + qbbar - qc - qcbar, 
     li1]*MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - q - qb, MB]*
  PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
  PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 - q + qb + qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*MetricTensor[li6, li7]*
   (FourVector[2*q + qc + qcbar, li9]*MetricTensor[li3, li7] + 
    FourVector[-q + qc + qcbar, li7]*MetricTensor[li3, li9] + 
    FourVector[-q - 2*(qc + qcbar), li3]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb, MB]*
   PropagatorDenominator[-p2 + qb + qbbar, 0]*PropagatorDenominator[
    -qc - qcbar, 0]*PropagatorDenominator[q + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[-p2 + q + qb]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[-qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - q - qb, MB]*
   PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    qbbar + qc + qcbar, MB]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + qb]) . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-p1 - qc - qcbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 - p2 + q + qb + qbbar, li4]*MetricTensor[li1, li6] + 
    FourVector[p2 - q - qb - qbbar + qc + qcbar, li1]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[-p2 + 2*q + qb + qbbar, li9]*
     MetricTensor[li3, li7] + FourVector[-p2 - q + qb + qbbar, li7]*
     MetricTensor[li3, li9] + FourVector[2*p2 - q - 2*(qb + qbbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
    0]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], 
    Index[Gluon, 8], Index[Gluon, 9]]*SUNF[Index[Gluon, 7], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb, MB]*
  PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
   -p2 + qb + qbbar + qcbar, MC]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb, MB]*
  PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
   p2 - qb - qbbar - qc, MC]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[-q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
    q + qbbar + qc + qcbar, MB]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 + p2 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[-p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb, MB]*
  PropagatorDenominator[-p2 + qb + qbbar, 0]*PropagatorDenominator[-q - qc, 
   MC]*PropagatorDenominator[q + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
    0]*PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[
    -p2 + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[-q - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
    q + qbbar + qc + qcbar, MB]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 + p2 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[-p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb, MB]*
  PropagatorDenominator[-p2 + qb + qbbar, 0]*PropagatorDenominator[q + qcbar, 
   MC]*PropagatorDenominator[q + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb, MB]*PropagatorDenominator[p2 - qb - qbbar, 
    0]*PropagatorDenominator[p2 - qb - qbbar - qc, MC]*
   PropagatorDenominator[q + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[
    p2 - q - qb - qc - qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[
    p2 - q - qb - qc - qcbar, MB]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qc + qcbar, 0]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p2 + 2*q + qc + qcbar, li7]*MetricTensor[li3, li5] + 
   FourVector[-p2 - q + qc + qcbar, li5]*MetricTensor[li3, li7] + 
   FourVector[2*p2 - q - 2*(qc + qcbar), li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qc, MC]*
  PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[
   p2 - q - qb - qc - qcbar, MB]*PropagatorDenominator[-p2 + q + qc + qcbar, 
   0]*SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[p2 - q - qc, MC]*
   PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
   PropagatorDenominator[-p2 + q + qc + qcbar, 0]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[
    p2 - qb - qc - qcbar, MB]*PropagatorDenominator[p2 - q - qb - qc - qcbar, 
    MB]*PropagatorDenominator[-p2 + qc + qcbar, 0]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[p2 - qc - qcbar, 
    0]*PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
   PropagatorDenominator[-p2 + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[
    p2 - q - qb - qc - qcbar, MB]*PropagatorDenominator[-p2 + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qc + qcbar, 0]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p2 + 2*q + qc + qcbar, li7]*MetricTensor[li3, li5] + 
   FourVector[-p2 - q + qc + qcbar, li5]*MetricTensor[li3, li7] + 
   FourVector[2*p2 - q - 2*(qc + qcbar), li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qc - qcbar, 0]*
  PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
  PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 7], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[p2 - q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
   PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
    -p2 + q + qcbar, MC]*PropagatorDenominator[-p2 + q + qc + qcbar, 0]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qc - qcbar, MB]*
   PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
   PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
    -p2 + qc + qcbar, 0]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*
     SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p2 - q, li4]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li4] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-2*p2 + 2*q + qc + qcbar, li9]*MetricTensor[li5, li7] + 
    FourVector[p2 - q + qc + qcbar, li7]*MetricTensor[li5, li9] + 
    FourVector[p2 - q - 2*(qc + qcbar), li5]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    p2 - q - qb - qc - qcbar, MB]*PropagatorDenominator[-p2 + q + qc + qcbar, 
    0]*SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]]*
   SUNF[Index[Gluon, 8], Index[Gluon, 9], Index[Gluon, 10]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-p2 + q + qb]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[p2 - q - qb, MB]*PropagatorDenominator[-qc - qcbar, 
   0]*PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
  SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[-qb - qc - qcbar, MB]*PropagatorDenominator[
   p2 - q - qb - qc - qcbar, MB]*PropagatorDenominator[qc + qcbar, 0]*
  SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-qc - qcbar, 0]*
   PropagatorDenominator[-q - qb - qc - qcbar, MB]*
   PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + q + qb]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[p2 - q - qb, MB]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    p2 - q - qb - qc - qcbar, MB]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li4, li5]*(FourVector[-p2 - qc - qcbar, li6]*
    MetricTensor[li2, li4] + FourVector[2*p2 - qc - qcbar, li4]*
    MetricTensor[li2, li6] + FourVector[-p2 + 2*(qc + qcbar), li2]*
    MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[p2 - qc - qcbar, 
   0]*PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
  PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
   Index[Gluon, 9]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
    SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li4, li5]*(FourVector[2*q + qc + qcbar, li7]*
    MetricTensor[li3, li5] + FourVector[-q + qc + qcbar, li5]*
    MetricTensor[li3, li7] + FourVector[-q - 2*(qc + qcbar), li3]*
    MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   -q - qb - qc - qcbar, MB]*PropagatorDenominator[p2 - q - qb - qc - qcbar, 
   MB]*PropagatorDenominator[q + qc + qcbar, 0]*
  SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qc - qcbar, MB]*PropagatorDenominator[
    -q - qb - qc - qcbar, MB]*PropagatorDenominator[p2 - q - qb - qc - qcbar, 
    MB]*PropagatorDenominator[qc + qcbar, 0]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-2*p2 + q + qc + qcbar, li6]*MetricTensor[li2, li4] + 
    FourVector[p2 + q + qc + qcbar, li4]*MetricTensor[li2, li6] + 
    FourVector[p2 - 2*(q + qc + qcbar), li2]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[2*q + qc + qcbar, li9]*
     MetricTensor[li3, li7] + FourVector[-q + qc + qcbar, li7]*
     MetricTensor[li3, li9] + FourVector[-q - 2*(qc + qcbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    p2 - q - qc - qcbar, 0]*PropagatorDenominator[p2 - q - qb - qc - qcbar, 
    MB]*PropagatorDenominator[q + qc + qcbar, 0]*
   SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li8]*
     SUNT[Index[Gluon, 10]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-p2 - qc - qcbar, li6]*MetricTensor[li2, li4] + 
    FourVector[2*p2 - qc - qcbar, li4]*MetricTensor[li2, li6] + 
    FourVector[-p2 + 2*(qc + qcbar), li2]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[p2 + q - qc - qcbar, li9]*
     MetricTensor[li3, li7] + FourVector[p2 - 2*q - qc - qcbar, li7]*
     MetricTensor[li3, li9] + FourVector[-2*p2 + q + 2*(qc + qcbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[
    p2 - q - qb - qc - qcbar, MB]*PropagatorDenominator[qc + qcbar, 0]*
   PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
   SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li4, li5]*(FourVector[-2*p2 + qc + qcbar, li6]*
    MetricTensor[li2, li4] + FourVector[p2 + qc + qcbar, li4]*
    MetricTensor[li2, li6] + FourVector[p2 - 2*(qc + qcbar), li2]*
    MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[
   p2 - qb - qc - qcbar, MB]*PropagatorDenominator[p2 - q - qb - qc - qcbar, 
   MB]*PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 2], 
   Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qc - qcbar, MB]*PropagatorDenominator[
    p2 - qb - qc - qcbar, MB]*PropagatorDenominator[p2 - q - qb - qc - qcbar, 
    MB]*PropagatorDenominator[qc + qcbar, 0]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[p2 - q - qc, MC]*PropagatorDenominator[
   p2 - q - qb - qc - qcbar, MB]*PropagatorDenominator[-p2 + q + qc + qcbar, 
   0]*SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - q - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
  PropagatorDenominator[-p2 + q + qcbar, MC]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[
    -q - qb - qc - qcbar, MB]*PropagatorDenominator[p2 - q - qb - qc - qcbar, 
    MB]*PropagatorDenominator[q + qc + qcbar, 0]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-2*p2 + q + qc + qcbar, li6]*MetricTensor[li2, li4] + 
   FourVector[p2 + q + qc + qcbar, li4]*MetricTensor[li2, li6] + 
   FourVector[p2 - 2*(q + qc + qcbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qc, MC]*
  PropagatorDenominator[p2 - q - qc - qcbar, 0]*
  PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
  PropagatorDenominator[q + qc + qcbar, 0]*SUNF[Index[Gluon, 2], 
   Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[p2 - q - qc, MC]*
   PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
   PropagatorDenominator[-p2 + q + qc + qcbar, 0]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb - qc - qcbar, MB]*
   PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
   PropagatorDenominator[q + qcbar, MC]*PropagatorDenominator[q + qc + qcbar, 
    0]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-2*p2 + q + qc + qcbar, li6]*MetricTensor[li2, li4] + 
   FourVector[p2 + q + qc + qcbar, li4]*MetricTensor[li2, li6] + 
   FourVector[p2 - 2*(q + qc + qcbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q - qc - qcbar, 0]*
  PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
  PropagatorDenominator[q + qcbar, MC]*PropagatorDenominator[q + qc + qcbar, 
   0]*SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + q + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[p2 - q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - q - qb - qc - qcbar, MB]*
   PropagatorDenominator[q + qcbar, MC]*PropagatorDenominator[
    -p2 + q + qcbar, MC]*PropagatorDenominator[-p2 + q + qc + qcbar, 0]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-p2 + qbbar, MB]*
   PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
    -p2 + qbbar, MB]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[p2 + q - qb - qbbar, li7]*
    MetricTensor[li3, li5] + FourVector[p2 - 2*q - qb - qbbar, li5]*
    MetricTensor[li3, li7] + FourVector[-2*p2 + q + 2*(qb + qbbar), li3]*
    MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
   -p2 + qbbar, MB]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
  PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]*
  SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - q - qb - qbbar, 0]*
   PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
    -p2 + q + qbbar, MB]*PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, 
    MC]), -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
    -p2 + qbbar, MB]*PropagatorDenominator[-p2 + qb + qbbar + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-p2 + qbbar, MB]*
   PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
    -p2 + qbbar, MB]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[p2 + q - qb - qbbar, li7]*
    MetricTensor[li3, li5] + FourVector[p2 - 2*q - qb - qbbar, li5]*
    MetricTensor[li3, li7] + FourVector[-2*p2 + q + 2*(qb + qbbar), li3]*
    MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
   -p2 + qbbar, MB]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
  PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]*
  SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - q - qb - qbbar, 0]*
   PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
    -p2 + q + qbbar, MB]*PropagatorDenominator[p2 - q - qb - qbbar - qc, 
    MC]), -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
    -p2 + qbbar, MB]*PropagatorDenominator[p2 - qb - qbbar - qc, MC]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[p2 - qbbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-p2 + qbbar, MB]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   -p2 + qbbar + qc + qcbar, MB]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*
     SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[2*p2 - 2*qb - 2*qbbar - qc - qcbar, li9]*
     MetricTensor[li5, li7] + FourVector[-p2 + qb + qbbar + 2*qc + 2*qcbar, 
      li7]*MetricTensor[li5, li9] + FourVector[-p2 + qb + qbbar - qc - qcbar, 
      li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
    -p2 + qbbar, MB]*PropagatorDenominator[-qc - qcbar, 0]*
   PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]]*
   SUNF[Index[Gluon, 8], Index[Gluon, 9], Index[Gluon, 10]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - qbbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-p2 + qbbar, MB]*
  PropagatorDenominator[-qb - qc - qcbar, MB]*PropagatorDenominator[
   qc + qcbar, 0]*PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-p2 + qbbar, MB]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    -q - qb - qc - qcbar, MB]), -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-p2 + qbbar, MB]*
   PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
    -p2 + qbbar + qc + qcbar, MB]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
  (FourVector[-p1 - qc - qcbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + q + qb + qbbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 - q - qb - qbbar + qc + qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qb, MB]*
  PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
   -p2 + q + qb + qbbar, 0]*PropagatorDenominator[qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - qbbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li4, li5]*(FourVector[2*q + qc + qcbar, li7]*
    MetricTensor[li3, li5] + FourVector[-q + qc + qcbar, li5]*
    MetricTensor[li3, li7] + FourVector[-q - 2*(qc + qcbar), li3]*
    MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[-qc - qcbar, 
   0]*PropagatorDenominator[-q - qb - qc - qcbar, MB]*
  PropagatorDenominator[q + qc + qcbar, 0]*SUNF[Index[Gluon, 7], 
   Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
    -qb - qc - qcbar, MB]*PropagatorDenominator[-q - qb - qc - qcbar, MB]*
   PropagatorDenominator[qc + qcbar, 0]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[p2 - q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[p2 - qbbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li4, li5]*(FourVector[-p1 + p2 - q - qb - qbbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 + qc + qcbar, li4]*
    MetricTensor[li1, li6] + FourVector[-p2 + q + qb + qbbar - qc - qcbar, 
     li1]*MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
   -p2 + q + qbbar, MB]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
  PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 - q + qb + qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*MetricTensor[li6, li7]*
   (FourVector[2*q + qc + qcbar, li9]*MetricTensor[li3, li7] + 
    FourVector[-q + qc + qcbar, li7]*MetricTensor[li3, li9] + 
    FourVector[-q - 2*(qc + qcbar), li3]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-p2 + qbbar, MB]*
   PropagatorDenominator[-p2 + qb + qbbar, 0]*PropagatorDenominator[
    -qc - qcbar, 0]*PropagatorDenominator[q + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
    -p2 + q + qbbar, MB]*PropagatorDenominator[-qb - qc - qcbar, MB]*
   PropagatorDenominator[qc + qcbar, 0]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li8]*
     SUNT[Index[Gluon, 10]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-p1 - qc - qcbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 - p2 + q + qb + qbbar, li4]*MetricTensor[li1, li6] + 
    FourVector[p2 - q - qb - qbbar + qc + qcbar, li1]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[-p2 + 2*q + qb + qbbar, li9]*
     MetricTensor[li3, li7] + FourVector[-p2 - q + qb + qbbar, li7]*
     MetricTensor[li3, li9] + FourVector[2*p2 - q - 2*(qb + qbbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
    -p2 + qbbar, MB]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], 
    Index[Gluon, 8], Index[Gluon, 9]]*SUNF[Index[Gluon, 7], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
  PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
   -p2 + qb + qbbar + qcbar, MC]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
  PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
   p2 - qb - qbbar - qc, MC]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[-q - qb - qc - qcbar, MB]*
   PropagatorDenominator[q + qc + qcbar, 0]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 + p2 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[-p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-p2 + qbbar, MB]*
  PropagatorDenominator[-p2 + qb + qbbar, 0]*PropagatorDenominator[-q - qc, 
   MC]*PropagatorDenominator[q + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
    -p2 + qbbar, MB]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[-p2 + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[q + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + qbbar, MB]*PropagatorDenominator[
    -q - qb - qc - qcbar, MB]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[q + qc + qcbar, 0]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 + p2 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[-p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-p2 + qbbar, MB]*
  PropagatorDenominator[-p2 + qb + qbbar, 0]*PropagatorDenominator[q + qcbar, 
   MC]*PropagatorDenominator[q + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[p2 - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
    -p2 + qbbar, MB]*PropagatorDenominator[p2 - qb - qbbar - qc, MC]*
   PropagatorDenominator[q + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qb - qbbar - 
       qcbar]) . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-p2 + qcbar, 
    MC]*PropagatorDenominator[-p2 + qb + qbbar + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qb - qbbar - 
       qcbar]) . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-p2 + qcbar, 
    MC]*PropagatorDenominator[-p2 + q + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[q - qb - qbbar, li6]*MetricTensor[li3, li5] + 
   FourVector[-2*q - qb - qbbar, li5]*MetricTensor[li3, li6] + 
   FourVector[q + 2*(qb + qbbar), li3]*MetricTensor[li5, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[
   -p2 + qcbar, MC]*PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]*
  SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qb - qbbar - 
       qcbar]) . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p2 - q, li4]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li4] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-p2 + q - qb - qbbar, li9]*MetricTensor[li5, li7] + 
    FourVector[2*p2 - 2*q - qb - qbbar, li7]*MetricTensor[li5, li9] + 
    FourVector[-p2 + q + 2*(qb + qbbar), li5]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -p2 + q + qb + qbbar, 0]*PropagatorDenominator[
    -p2 + q + qb + qbbar + qcbar, MC]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 8], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - q - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
   -p2 + q + qcbar, MC]*PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, 
   MC]*SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
   qb + qbbar + qcbar, MC]*PropagatorDenominator[
   -p2 + q + qb + qbbar + qcbar, MC]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qb - qbbar - 
       qcbar]) . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-q - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[q + qb + qbbar + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p2 - qb - qbbar, li6]*MetricTensor[li2, li4] + 
   FourVector[2*p2 - qb - qbbar, li4]*MetricTensor[li2, li6] + 
   FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[q + qcbar, MC]*
  PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qb - qbbar - 
       qcbar]) . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - q - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qcbar, MC]*PropagatorDenominator[
    -p2 + q + qb + qbbar + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-q - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[q - qb - qbbar, li7]*MetricTensor[li3, li5] + 
   FourVector[-2*q - qb - qbbar, li5]*MetricTensor[li3, li7] + 
   FourVector[q + 2*(qb + qbbar), li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[q + qb + qbbar, 0]*PropagatorDenominator[
   q + qb + qbbar + qcbar, MC]*PropagatorDenominator[
   -p2 + q + qb + qbbar + qcbar, MC]*SUNF[Index[Gluon, 7], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qb - qbbar - 
       qcbar]) . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-q - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    qb + qbbar + qcbar, MC]*PropagatorDenominator[q + qb + qbbar + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qb - qbbar - 
       qcbar]) . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-p2 - qb - qbbar, li6]*MetricTensor[li2, li4] + 
    FourVector[2*p2 - qb - qbbar, li4]*MetricTensor[li2, li6] + 
    FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[p2 + q - qb - qbbar, li9]*
     MetricTensor[li3, li7] + FourVector[p2 - 2*q - qb - qbbar, li7]*
     MetricTensor[li3, li9] + FourVector[-2*p2 + q + 2*(qb + qbbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
    qb + qbbar, 0]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]*
   SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qb - qbbar - 
       qcbar]) . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-2*p2 + q + qb + qbbar, li6]*MetricTensor[li2, li4] + 
    FourVector[p2 + q + qb + qbbar, li4]*MetricTensor[li2, li6] + 
    FourVector[p2 - 2*(q + qb + qbbar), li2]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[2*q + qb + qbbar, li9]*
     MetricTensor[li3, li7] + FourVector[-q + qb + qbbar, li7]*
     MetricTensor[li3, li9] + FourVector[-q - 2*(qb + qbbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    p2 - q - qb - qbbar, 0]*PropagatorDenominator[q + qb + qbbar, 0]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]*
   SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p2 - qb - qbbar, li6]*MetricTensor[li2, li4] + 
   FourVector[2*p2 - qb - qbbar, li4]*MetricTensor[li2, li6] + 
   FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[
   -p2 + qb + qbbar + qcbar, MC]*PropagatorDenominator[
   -p2 + q + qb + qbbar + qcbar, MC]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qb - qbbar - 
       qcbar]) . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    qb + qbbar + qcbar, MC]*PropagatorDenominator[-p2 + qb + qbbar + qcbar, 
    MC]*PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[p2 - qb - qbbar - qc, MC]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[p2 - q - qc, MC]*PropagatorDenominator[
    p2 - q - qb - qbbar - qc, MC]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[q - qb - qbbar, li6]*
    MetricTensor[li3, li5] + FourVector[-2*q - qb - qbbar, li5]*
    MetricTensor[li3, li6] + FourVector[q + 2*(qb + qbbar), li3]*
    MetricTensor[li5, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
   -q - qb - qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
  PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]*
  SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[-p2 + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[
   p2 - qb - qbbar - qc, MC]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[p2 - 2*qb - 2*qbbar - qc - qcbar, li9]*
     MetricTensor[li5, li7] + FourVector[-2*p2 + qb + qbbar + 2*qc + 2*qcbar, 
      li7]*MetricTensor[li5, li9] + FourVector[p2 + qb + qbbar - qc - qcbar, 
      li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[
    -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 8], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[
   qb + qbbar + qcbar, MC]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[-q - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[q + qcbar, MC]*PropagatorDenominator[
    q + qb + qbbar + qcbar, MC]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qb + qbbar, 0]*
  PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[q + qcbar, MC]*
  PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[p2 - qb - qbbar - qc, MC]*
   PropagatorDenominator[q + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[-q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[q - qb - qbbar, li7]*
    MetricTensor[li3, li5] + FourVector[-2*q - qb - qbbar, li5]*
    MetricTensor[li3, li7] + FourVector[q + 2*(qb + qbbar), li3]*
    MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[q + qb + qbbar, 
   0]*PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[
   q + qb + qbbar + qcbar, MC]*SUNF[Index[Gluon, 7], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[-q - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[qb + qbbar + qcbar, MC]*
   PropagatorDenominator[q + qb + qbbar + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-p2 + q + qc]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[-p1 - qb - qbbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 - p2 + q + qc + qcbar, li4]*
    MetricTensor[li1, li6] + FourVector[p2 - q + qb + qbbar - qc - qcbar, 
     li1]*MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
  PropagatorDenominator[p2 - q - qc, MC]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-p1 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 - p2 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[-p2 + 2*q + qc + qcbar, li9]*
     MetricTensor[li3, li7] + FourVector[-p2 - q + qc + qcbar, li7]*
     MetricTensor[li3, li9] + FourVector[2*p2 - q - 2*(qc + qcbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[
    -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
    Index[Gluon, 9]]*SUNF[Index[Gluon, 7], Index[Gluon, 9], 
    Index[Gluon, 10]]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - qc - qcbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + q + qb + qbbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 - q - qb - qbbar + qc + qcbar, li1]*
     MetricTensor[li4, li6])*MetricTensor[li6, li7]*
   (FourVector[2*q + qb + qbbar, li9]*MetricTensor[li3, li7] + 
    FourVector[-q + qb + qbbar, li7]*MetricTensor[li3, li9] + 
    FourVector[-q - 2*(qb + qbbar), li3]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
   PropagatorDenominator[q + qb + qbbar, 0]*PropagatorDenominator[p2 - qc, 
    MC]*PropagatorDenominator[-p2 + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + qc]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[p2 - q - qc, MC]*PropagatorDenominator[
    qb + qbbar + qcbar, MC]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p2 - q, li4]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li4] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li4])*MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-p2 + q - qb - qbbar, li9]*MetricTensor[li5, li7] + 
    FourVector[2*p2 - 2*q - qb - qbbar, li7]*MetricTensor[li5, li9] + 
    FourVector[-p2 + q + 2*(qb + qbbar), li5]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -p2 + q + qb + qbbar, 0]*PropagatorDenominator[p2 - q - qb - qbbar - qc, 
    MC]*SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]]*
   SUNF[Index[Gluon, 8], Index[Gluon, 9], Index[Gluon, 10]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[p2 - q - qc, 
   MC]*PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]*
  SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
   -qb - qbbar - qc, MC]*PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]*
  SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[q + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[-q - qb - qbbar - qc, MC]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[-p2 - qb - qbbar, li6]*
    MetricTensor[li2, li4] + FourVector[2*p2 - qb - qbbar, li4]*
    MetricTensor[li2, li6] + FourVector[-p2 + 2*(qb + qbbar), li2]*
    MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[qb + qbbar, 
   0]*PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[
   p2 - q - qb - qbbar - qc, MC]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[p2 - q - qc, MC]*PropagatorDenominator[
    p2 - q - qb - qbbar - qc, MC]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[q - qb - qbbar, li7]*
    MetricTensor[li3, li5] + FourVector[-2*q - qb - qbbar, li5]*
    MetricTensor[li3, li7] + FourVector[q + 2*(qb + qbbar), li3]*
    MetricTensor[li5, li7])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[q + qb + qbbar, 
   0]*PropagatorDenominator[-q - qb - qbbar - qc, MC]*
  PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]*
  SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -qb - qbbar - qc, MC]*PropagatorDenominator[-q - qb - qbbar - qc, MC]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-p2 - qb - qbbar, li6]*MetricTensor[li2, li4] + 
    FourVector[2*p2 - qb - qbbar, li4]*MetricTensor[li2, li6] + 
    FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[p2 + q - qb - qbbar, li9]*
     MetricTensor[li3, li7] + FourVector[p2 - 2*q - qb - qbbar, li7]*
     MetricTensor[li3, li9] + FourVector[-2*p2 + q + 2*(qb + qbbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[
    qb + qbbar, 0]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]*
   SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-2*p2 + q + qb + qbbar, li6]*MetricTensor[li2, li4] + 
    FourVector[p2 + q + qb + qbbar, li4]*MetricTensor[li2, li6] + 
    FourVector[p2 - 2*(q + qb + qbbar), li2]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[2*q + qb + qbbar, li9]*
     MetricTensor[li3, li7] + FourVector[-q + qb + qbbar, li7]*
     MetricTensor[li3, li9] + FourVector[-q - 2*(qb + qbbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    p2 - q - qb - qbbar, 0]*PropagatorDenominator[q + qb + qbbar, 0]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]*
   SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[-p2 - qb - qbbar, li6]*
    MetricTensor[li2, li4] + FourVector[2*p2 - qb - qbbar, li4]*
    MetricTensor[li2, li6] + FourVector[-p2 + 2*(qb + qbbar), li2]*
    MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[qb + qbbar, 
   0]*PropagatorDenominator[p2 - qb - qbbar - qc, MC]*
  PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -qb - qbbar - qc, MC]*PropagatorDenominator[p2 - qb - qbbar - qc, MC]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[p2 - qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
   -p2 + qb + qbbar + qcbar, MC]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[p2 - 2*qb - 2*qbbar - qc - qcbar, li9]*
     MetricTensor[li5, li7] + FourVector[-2*p2 + qb + qbbar + 2*qc + 2*qcbar, 
      li7]*MetricTensor[li5, li9] + FourVector[p2 + qb + qbbar - qc - qcbar, 
      li5]*MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    p2 - qc - qcbar, 0]*PropagatorDenominator[-p2 + qcbar, MC]*
   PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]]*
   SUNF[Index[Gluon, 8], Index[Gluon, 9], Index[Gluon, 10]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[p2 - qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qb - qbbar - qc, MC]*PropagatorDenominator[
   -p2 + qcbar, MC]*PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[q + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[-q - qb - qbbar - qc, MC]*
   PropagatorDenominator[-p2 + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[p2 - qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[-p1 - qb - qbbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 - p2 + q + qc + qcbar, li4]*
    MetricTensor[li1, li6] + FourVector[p2 - q + qb + qbbar - qc - qcbar, 
     li1]*MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[-q - qc, MC]*
  PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
    -p2 + qb + qbbar + qcbar, MC]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[q - qb - qbbar, li7]*MetricTensor[li3, li5] + 
   FourVector[-2*q - qb - qbbar, li5]*MetricTensor[li3, li7] + 
   FourVector[q + 2*(qb + qbbar), li3]*MetricTensor[li5, li7])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[q + qb + qbbar, 0]*PropagatorDenominator[
   -q - qb - qbbar - qc, MC]*PropagatorDenominator[-p2 + qcbar, MC]*
  SUNF[Index[Gluon, 7], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MC + DiracSlash[q + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -qb - qbbar - qc, MC]*PropagatorDenominator[-q - qb - qbbar - qc, MC]*
   PropagatorDenominator[-p2 + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[p2 - qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[-p1 - qb - qbbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 - p2 + q + qc + qcbar, li4]*
    MetricTensor[li1, li6] + FourVector[p2 - q + qb + qbbar - qc - qcbar, 
     li1]*MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[-p2 + qcbar, MC]*
  PropagatorDenominator[-p2 + q + qcbar, MC]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li8]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-p1 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 - p2 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*(FourVector[-p2 + 2*q + qc + qcbar, li9]*
     MetricTensor[li3, li7] + FourVector[-p2 - q + qc + qcbar, li7]*
     MetricTensor[li3, li9] + FourVector[2*p2 - q - 2*(qc + qcbar), li3]*
     MetricTensor[li7, li9])*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[
    p2 - qc - qcbar, 0]*PropagatorDenominator[-p2 + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - qc - qcbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + q + qb + qbbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 - q - qb - qbbar + qc + qcbar, li1]*
     MetricTensor[li4, li6])*MetricTensor[li6, li7]*
   (FourVector[2*q + qb + qbbar, li9]*MetricTensor[li3, li7] + 
    FourVector[-q + qb + qbbar, li7]*MetricTensor[li3, li9] + 
    FourVector[-q - 2*(qb + qbbar), li3]*MetricTensor[li7, li9])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
   PropagatorDenominator[q + qb + qbbar, 0]*PropagatorDenominator[
    -p2 + qcbar, MC]*PropagatorDenominator[-p2 + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 10]]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -qb - qbbar - qc, MC]*PropagatorDenominator[-p2 + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qcbar, MC]), 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li10]*
    SUNT[Index[Gluon, 11]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li10, li11]*(FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  (FourVector[-2*p2 + qb + qbbar + qc + qcbar, li6]*MetricTensor[li2, li5] + 
   FourVector[p2 + qb + qbbar + qc + qcbar, li5]*MetricTensor[li2, li6] + 
   FourVector[p2 - 2*(qb + qbbar + qc + qcbar), li2]*MetricTensor[li5, li6])*
  MetricTensor[li6, li7]*(FourVector[qb + qbbar + 2*(qc + qcbar), li9]*
    MetricTensor[li11, li7] + FourVector[qb + qbbar - qc - qcbar, li7]*
    MetricTensor[li11, li9] + FourVector[-2*qb - 2*qbbar - qc - qcbar, li11]*
    MetricTensor[li7, li9])*MetricTensor[li8, li9]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-qc - qcbar, 0]*
  PropagatorDenominator[qb + qbbar + qc + qcbar, 0]*
  PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]*
  SUNF[Index[Gluon, 9], Index[Gluon, 10], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li10]*SUNT[Index[Gluon, 11]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li10, li11]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  (FourVector[-p2 + qb + qbbar + 2*qc + 2*qcbar, li9]*
    MetricTensor[li11, li5] + FourVector[-p2 + qb + qbbar - qc - qcbar, li5]*
    MetricTensor[li11, li9] + FourVector[2*p2 - 2*qb - 2*qbbar - qc - qcbar, 
     li11]*MetricTensor[li5, li9])*MetricTensor[li6, li7]*
  (FourVector[-p2 - qb - qbbar, li8]*MetricTensor[li2, li6] + 
   FourVector[2*p2 - qb - qbbar, li6]*MetricTensor[li2, li8] + 
   FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li6, li8])*
  MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[-qc - qcbar, 0]*
  PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]]*
  SUNF[Index[Gluon, 2], Index[Gluon, 9], Index[Gluon, 10]]*
  SUNF[Index[Gluon, 8], Index[Gluon, 10], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li10]*SUNT[Index[Gluon, 11]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li10, li11]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  (FourVector[-p2 + 2*qb + 2*qbbar + qc + qcbar, li9]*
    MetricTensor[li11, li5] + FourVector[-p2 - qb - qbbar + qc + qcbar, li5]*
    MetricTensor[li11, li9] + FourVector[2*p2 - qb - qbbar - 2*qc - 2*qcbar, 
     li11]*MetricTensor[li5, li9])*MetricTensor[li6, li7]*
  (FourVector[-p2 - qc - qcbar, li8]*MetricTensor[li2, li6] + 
   FourVector[2*p2 - qc - qcbar, li6]*MetricTensor[li2, li8] + 
   FourVector[-p2 + 2*(qc + qcbar), li2]*MetricTensor[li6, li8])*
  MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[qc + qcbar, 
   0]*PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]]*
  SUNF[Index[Gluon, 2], Index[Gluon, 9], Index[Gluon, 10]]*
  SUNF[Index[Gluon, 8], Index[Gluon, 10], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li10]*
    SUNT[Index[Gluon, 11]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li10, li11]*(FourVector[-p2 - q, li5]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li5] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li5])*MetricTensor[li4, li5]*
  (FourVector[-p1 + p2 - q, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 + qb + qbbar + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[-p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*(FourVector[qb + qbbar + 2*(qc + qcbar), li9]*
    MetricTensor[li11, li7] + FourVector[qb + qbbar - qc - qcbar, li7]*
    MetricTensor[li11, li9] + FourVector[-2*qb - 2*qbbar - qc - qcbar, li11]*
    MetricTensor[li7, li9])*MetricTensor[li8, li9]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-p2 + q, 0]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]]*
  SUNF[Index[Gluon, 9], Index[Gluon, 10], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li10]*SUNT[Index[Gluon, 11]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li10, li11]*
  (FourVector[-p2 - q, li8]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li8] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li8])*MetricTensor[li4, li5]*
  (FourVector[-p1 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*(FourVector[-p2 + q + 2*(qc + qcbar), li9]*
    MetricTensor[li11, li7] + FourVector[-p2 + q - qc - qcbar, li7]*
    MetricTensor[li11, li9] + FourVector[2*p2 - 2*q - qc - qcbar, li11]*
    MetricTensor[li7, li9])*MetricTensor[li8, li9]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - q, 0]*PropagatorDenominator[qb + qbbar, 0]*
  PropagatorDenominator[-qc - qcbar, 0]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 10]]*
  SUNF[Index[Gluon, 9], Index[Gluon, 10], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li10]*SUNT[Index[Gluon, 11]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li10, li11]*
  (FourVector[-p2 - q, li8]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li8] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li8])*MetricTensor[li4, li5]*
  (FourVector[-p1 - qc - qcbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + q + qb + qbbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 - q - qb - qbbar + qc + qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*(FourVector[-p2 + q + 2*(qb + qbbar), li9]*
    MetricTensor[li11, li7] + FourVector[-p2 + q - qb - qbbar, li7]*
    MetricTensor[li11, li9] + FourVector[2*p2 - 2*q - qb - qbbar, li11]*
    MetricTensor[li7, li9])*MetricTensor[li8, li9]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - q, 0]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
  PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 10]]*
  SUNF[Index[Gluon, 9], Index[Gluon, 10], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li10]*SUNT[Index[Gluon, 11]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li10, li11]*
  (FourVector[-q + qc + qcbar, li9]*MetricTensor[li11, li3] + 
   FourVector[-q - 2*(qc + qcbar), li3]*MetricTensor[li11, li9] + 
   FourVector[2*q + qc + qcbar, li11]*MetricTensor[li3, li9])*
  MetricTensor[li4, li5]*(FourVector[-p1 + p2 - q - qc - qcbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 + qb + qbbar, li4]*
    MetricTensor[li1, li6] + FourVector[-p2 + q - qb - qbbar + qc + qcbar, 
     li1]*MetricTensor[li4, li6])*(FourVector[-2*p2 + q + qc + qcbar, li8]*
    MetricTensor[li2, li5] + FourVector[p2 + q + qc + qcbar, li5]*
    MetricTensor[li2, li8] + FourVector[p2 - 2*(q + qc + qcbar), li2]*
    MetricTensor[li5, li8])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[-qc - qcbar, 0]*
  PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 10]]*
  SUNF[Index[Gluon, 7], Index[Gluon, 10], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li10]*SUNT[Index[Gluon, 11]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li10, li11]*
  (FourVector[-q + qb + qbbar, li9]*MetricTensor[li11, li3] + 
   FourVector[-q - 2*(qb + qbbar), li3]*MetricTensor[li11, li9] + 
   FourVector[2*q + qb + qbbar, li11]*MetricTensor[li3, li9])*
  MetricTensor[li4, li5]*(FourVector[-p1 + p2 - q - qb - qbbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 + qc + qcbar, li4]*
    MetricTensor[li1, li6] + FourVector[-p2 + q + qb + qbbar - qc - qcbar, 
     li1]*MetricTensor[li4, li6])*(FourVector[-2*p2 + q + qb + qbbar, li8]*
    MetricTensor[li2, li5] + FourVector[p2 + q + qb + qbbar, li5]*
    MetricTensor[li2, li8] + FourVector[p2 - 2*(q + qb + qbbar), li2]*
    MetricTensor[li5, li8])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[q + qb + qbbar, 
   0]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
  PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 10]]*
  SUNF[Index[Gluon, 7], Index[Gluon, 10], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li10]*
    SUNT[Index[Gluon, 11]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li10, li11]*(FourVector[-q + qc + qcbar, li7]*
    MetricTensor[li11, li3] + FourVector[-q - 2*(qc + qcbar), li3]*
    MetricTensor[li11, li7] + FourVector[2*q + qc + qcbar, li11]*
    MetricTensor[li3, li7])*MetricTensor[li4, li5]*
  (FourVector[-p1 + p2 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[-p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  (FourVector[-2*p2 + qb + qbbar, li8]*MetricTensor[li2, li5] + 
   FourVector[p2 + qb + qbbar, li5]*MetricTensor[li2, li8] + 
   FourVector[p2 - 2*(qb + qbbar), li2]*MetricTensor[li5, li8])*
  MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qb + qbbar, 0]*
  PropagatorDenominator[-p2 + qb + qbbar, 0]*PropagatorDenominator[
   -qc - qcbar, 0]*PropagatorDenominator[q + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 10]]*
  SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li10]*SUNT[Index[Gluon, 11]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li9]*
    SUNT[Index[Gluon, 10]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li10, li11]*(FourVector[-q + qb + qbbar, li7]*
    MetricTensor[li11, li3] + FourVector[-q - 2*(qb + qbbar), li3]*
    MetricTensor[li11, li7] + FourVector[2*q + qb + qbbar, li11]*
    MetricTensor[li3, li7])*MetricTensor[li4, li5]*
  (FourVector[-p1 + p2 - qc - qcbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 + q + qb + qbbar, li4]*MetricTensor[li1, li6] + 
   FourVector[-p2 - q - qb - qbbar + qc + qcbar, li1]*MetricTensor[li4, li6])*
  (FourVector[-2*p2 + qc + qcbar, li8]*MetricTensor[li2, li5] + 
   FourVector[p2 + qc + qcbar, li5]*MetricTensor[li2, li8] + 
   FourVector[p2 - 2*(qc + qcbar), li2]*MetricTensor[li5, li8])*
  MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[q + qb + qbbar, 0]*PropagatorDenominator[qc + qcbar, 
   0]*PropagatorDenominator[-p2 + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 10]]*
  SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qcbar, MC]*MetricTensor[li10, li11]*
  (FourVector[2*p2 - qc - qcbar, li8]*MetricTensor[li10, li2] + 
   FourVector[-p2 + 2*(qc + qcbar), li2]*MetricTensor[li10, li8] + 
   FourVector[-p2 - qc - qcbar, li10]*MetricTensor[li2, li8])*
  (FourVector[-p2 - q + qc + qcbar, li7]*MetricTensor[li11, li3] + 
   FourVector[2*p2 - q - 2*(qc + qcbar), li3]*MetricTensor[li11, li7] + 
   FourVector[-p2 + 2*q + qc + qcbar, li11]*MetricTensor[li3, li7])*
  MetricTensor[li4, li5]*(FourVector[-p1 - qb - qbbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 - p2 + q + qc + qcbar, li4]*
    MetricTensor[li1, li6] + FourVector[p2 - q + qb + qbbar - qc - qcbar, 
     li1]*MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qb + qbbar, 0]*
  PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[qc + qcbar, 
   0]*PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
  SUNF[Index[Gluon, 2], Index[Gluon, 10], Index[Gluon, 11]]*
  SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 11]], 
 GS^3*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li10, li11]*
  (FourVector[2*p2 - qb - qbbar, li8]*MetricTensor[li10, li2] + 
   FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li10, li8] + 
   FourVector[-p2 - qb - qbbar, li10]*MetricTensor[li2, li8])*
  (FourVector[-p2 - q + qb + qbbar, li7]*MetricTensor[li11, li3] + 
   FourVector[2*p2 - q - 2*(qb + qbbar), li3]*MetricTensor[li11, li7] + 
   FourVector[-p2 + 2*q + qb + qbbar, li11]*MetricTensor[li3, li7])*
  MetricTensor[li4, li5]*(FourVector[-p1 - qc - qcbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 - p2 + q + qb + qbbar, li4]*
    MetricTensor[li1, li6] + FourVector[p2 - q - qb - qbbar + qc + qcbar, 
     li1]*MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[
   -p2 + q + qb + qbbar, 0]*PropagatorDenominator[qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
  SUNF[Index[Gluon, 2], Index[Gluon, 10], Index[Gluon, 11]]*
  SUNF[Index[Gluon, 7], Index[Gluon, 9], Index[Gluon, 11]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*
     SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*(FourVector[-2*p2 + qb + qbbar + qc + qcbar, li6]*
     MetricTensor[li2, li5] + FourVector[p2 + qb + qbbar + qc + qcbar, li5]*
     MetricTensor[li2, li6] + FourVector[p2 - 2*(qb + qbbar + qc + qcbar), 
      li2]*MetricTensor[li5, li6])*MetricTensor[li6, li7]*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
   PropagatorDenominator[qb + qbbar + qcbar, MC]*
   PropagatorDenominator[qb + qbbar + qc + qcbar, 0]*
   PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]]*
   SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li9]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*(FourVector[-2*p2 + qb + qbbar + qc + qcbar, li6]*
     MetricTensor[li2, li5] + FourVector[p2 + qb + qbbar + qc + qcbar, li5]*
     MetricTensor[li2, li6] + FourVector[p2 - 2*(qb + qbbar + qc + qcbar), 
      li2]*MetricTensor[li5, li6])*MetricTensor[li6, li7]*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
   PropagatorDenominator[-qb - qbbar - qc, MC]*PropagatorDenominator[
    qb + qbbar + qc + qcbar, 0]*PropagatorDenominator[
    -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
    Index[Gluon, 9]]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-p2 - qb - qbbar, li8]*MetricTensor[li2, li6] + 
    FourVector[2*p2 - qb - qbbar, li6]*MetricTensor[li2, li8] + 
    FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li6, li8])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
   PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[
    -p2 + qb + qbbar + qcbar, MC]*PropagatorDenominator[
    -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 2], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[qb + qbbar + qcbar, MC]*
  PropagatorDenominator[-p2 + qb + qbbar + qcbar, MC]*
  PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li9]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[-p2 + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-p2 - qb - qbbar, li8]*MetricTensor[li2, li6] + 
    FourVector[2*p2 - qb - qbbar, li6]*MetricTensor[li2, li8] + 
    FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li6, li8])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
   PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[
    p2 - qb - qbbar - qc, MC]*PropagatorDenominator[
    -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 2], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qbbar, 0]*
  PropagatorDenominator[-qb - qbbar - qc, MC]*PropagatorDenominator[
   p2 - qb - qbbar - qc, MC]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*
     SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p2 - q, li5]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li5] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li5])*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - q, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + qb + qbbar + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 + q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + q, 0]*PropagatorDenominator[-qb - qbbar, 0]*
   PropagatorDenominator[qb + qbbar + qcbar, MC]*
   PropagatorDenominator[qb + qbbar + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li9]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p2 - q, li5]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li5] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li5])*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - q, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + qb + qbbar + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 + q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + q, 0]*PropagatorDenominator[-qb - qbbar, 0]*
   PropagatorDenominator[-qb - qbbar - qc, MC]*PropagatorDenominator[
    qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
    Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
    Index[Gluon, 8]]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*
     SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[p2 - q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p2 - q, li8]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li8] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li8])*MetricTensor[li4, li5]*
   (FourVector[-p1 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 - p2 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
   PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[
    -p2 + q + qcbar, MC]*PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 10]]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li9]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[-p2 + q + qc]) . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*(FourVector[-p2 - q, li8]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li8] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li8])*MetricTensor[li4, li5]*
   (FourVector[-p1 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 - p2 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
   MetricTensor[li6, li7]*MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
   PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[p2 - q - qc, 
    MC]*PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 10]]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[p2 - q - qc, 
   MC]*PropagatorDenominator[qb + qbbar + qcbar, MC]*
  SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - q - qcbar]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
   -qb - qbbar - qc, MC]*PropagatorDenominator[-p2 + q + qcbar, MC]*
  SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - q - qc - qcbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + qb + qbbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 + q - qb - qbbar + qc + qcbar, li1]*
     MetricTensor[li4, li6])*(FourVector[-2*p2 + q + qc + qcbar, li8]*
     MetricTensor[li2, li5] + FourVector[p2 + q + qc + qcbar, li5]*
     MetricTensor[li2, li8] + FourVector[p2 - 2*(q + qc + qcbar), li2]*
     MetricTensor[li5, li8])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
    -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
    Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
    Index[Gluon, 10]]), -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[qb + qbbar + qcbar, MC]*
   PropagatorDenominator[-p2 + qb + qbbar + qcbar, MC]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 - q + qb + qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*(FourVector[-2*p2 + qb + qbbar, li8]*
     MetricTensor[li2, li5] + FourVector[p2 + qb + qbbar, li5]*
     MetricTensor[li2, li8] + FourVector[p2 - 2*(qb + qbbar), li2]*
     MetricTensor[li5, li8])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[
    -p2 + qb + qbbar, 0]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], 
    Index[Gluon, 8], Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
    Index[Gluon, 10]]), -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[-qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[-q - qc, MC]*
   PropagatorDenominator[p2 - q - qc, MC]*PropagatorDenominator[
    qb + qbbar + qcbar, MC]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MC + DiracSlash[q + qc]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + q + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 - q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qb + qbbar, 0]*
  PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[p2 - q - qc, MC]*
  PropagatorDenominator[-p2 + q + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MC + DiracSlash[q + qc]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[-p2 - qb - qbbar, li6]*
    MetricTensor[li2, li4] + FourVector[2*p2 - qb - qbbar, li4]*
    MetricTensor[li2, li6] + FourVector[-p2 + 2*(qb + qbbar), li2]*
    MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - qb - qbbar, 0]*PropagatorDenominator[qb + qbbar, 
   0]*PropagatorDenominator[-q - qc, MC]*PropagatorDenominator[
   -p2 + qb + qbbar + qcbar, MC]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
   Index[Gluon, 9]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li9]*
     SUNT[Index[Gluon, 10]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - q - qc - qcbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + qb + qbbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 + q - qb - qbbar + qc + qcbar, li1]*
     MetricTensor[li4, li6])*(FourVector[-2*p2 + q + qc + qcbar, li8]*
     MetricTensor[li2, li5] + FourVector[p2 + q + qc + qcbar, li5]*
     MetricTensor[li2, li8] + FourVector[p2 - 2*(q + qc + qcbar), li2]*
     MetricTensor[li5, li8])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[q + qc + qcbar, 0]*PropagatorDenominator[
    -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
    Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
    Index[Gluon, 10]]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*
     SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + q + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 - q + qb + qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*(FourVector[-2*p2 + qb + qbbar, li8]*
     MetricTensor[li2, li5] + FourVector[p2 + qb + qbbar, li5]*
     MetricTensor[li2, li8] + FourVector[p2 - 2*(qb + qbbar), li2]*
     MetricTensor[li5, li8])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[
    -p2 + qb + qbbar, 0]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], 
    Index[Gluon, 8], Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
    Index[Gluon, 10]]), -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -qb - qbbar - qc, MC]*PropagatorDenominator[q + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[-q - qcbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-qb - qbbar, 0]*PropagatorDenominator[
    -qb - qbbar - qc, MC]*PropagatorDenominator[p2 - qb - qbbar - qc, MC]*
   PropagatorDenominator[q + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - q - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-q - qcbar]) . 
   ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[-p1 - qb - qbbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 - p2 + q + qc + qcbar, li4]*
    MetricTensor[li1, li6] + FourVector[p2 - q + qb + qbbar - qc - qcbar, 
     li1]*MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[q + qcbar, MC]*
  PropagatorDenominator[-p2 + q + qcbar, MC]*PropagatorDenominator[
   -p2 + q + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   DiracSpinor[-qbbar, MB]*DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . (MC + DiracSlash[-p2 + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[-q - qcbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p2 - qb - qbbar, li6]*MetricTensor[li2, li4] + 
   FourVector[2*p2 - qb - qbbar, li4]*MetricTensor[li2, li6] + 
   FourVector[-p2 + 2*(qb + qbbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qbbar, 0]*
  PropagatorDenominator[qb + qbbar, 0]*PropagatorDenominator[
   p2 - qb - qbbar - qc, MC]*PropagatorDenominator[q + qcbar, MC]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-q - qb - qbbar, 
    0]*PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
    -p2 + q + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[q + qbbar, 
    MB]*PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
    -p2 + q + qb + qbbar + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-p2 + q + qb]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[p2 - q - qb, MB]*PropagatorDenominator[
   p2 - q - qb - qbbar, 0]*PropagatorDenominator[
   -p2 + q + qb + qbbar + qcbar, MC]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[p2 - q - qbbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[p2 - q - qb - qbbar, 0]*
  PropagatorDenominator[-p2 + q + qbbar, MB]*PropagatorDenominator[
   -p2 + q + qb + qbbar + qcbar, MC]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-q - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-q - qb - qbbar, 
    0]*PropagatorDenominator[q + qb + qbbar + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + q + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[p2 - q - qb, MB]*
   PropagatorDenominator[p2 - q - qb - qbbar, 0]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[-p2 - q - qb - qbbar, li6]*
    MetricTensor[li2, li4] + FourVector[2*p2 - q - qb - qbbar, li4]*
    MetricTensor[li2, li6] + FourVector[-p2 + 2*(q + qb + qbbar), li2]*
    MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[
   p2 - q - qb - qbbar, 0]*PropagatorDenominator[q + qb + qbbar, 0]*
  PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-q - qb - qbbar - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[q + qbbar, 
    MB]*PropagatorDenominator[q + qb + qbbar + qcbar, MC]*
   PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - q - qb - qbbar, 0]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[
    -p2 + q + qbbar, MB]*PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, 
    MC]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MC + DiracSlash[p2 - q - qb - qbbar - qcbar]) . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[-p2 - q - qb - qbbar, li6]*
    MetricTensor[li2, li4] + FourVector[2*p2 - q - qb - qbbar, li4]*
    MetricTensor[li2, li6] + FourVector[-p2 + 2*(q + qb + qbbar), li2]*
    MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - q - qb - qbbar, 0]*
  PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[q + qb + qbbar, 
   0]*PropagatorDenominator[-p2 + q + qb + qbbar + qcbar, MC]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-q - qb - qbbar, 
    0]*PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[
    p2 - q - qb - qbbar - qc, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[q + qbbar, 
    MB]*PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[
    p2 - q - qb - qbbar - qc, MC]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qc, MC]*
  PropagatorDenominator[-p2 + qc + qcbar, 0]*PropagatorDenominator[
   -p2 + qbbar + qc + qcbar, MB]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qc, MC]*
  PropagatorDenominator[p2 - qb - qc - qcbar, MB]*
  PropagatorDenominator[-p2 + qc + qcbar, 0]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[-q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-q - qb - qbbar, 
    0]*PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[
    q + qb + qbbar + qcbar, MC]), -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[-p2 + qc + qcbar, 0]*PropagatorDenominator[
    -p2 + qbbar + qc + qcbar, MB]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 - q - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 + q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qb, MB]*
  PropagatorDenominator[q + qb + qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
  PropagatorDenominator[-p2 + qc + qcbar, 0]*SUNF[Index[Gluon, 1], 
   Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[-q - qb - qbbar - qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[q + qbbar, 
    MB]*PropagatorDenominator[p2 - qc, MC]*PropagatorDenominator[
    q + qb + qbbar + qcbar, MC]), -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[p2 - qc, MC]*
   PropagatorDenominator[p2 - qb - qc - qcbar, MB]*
   PropagatorDenominator[-p2 + qc + qcbar, 0]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MC + DiracSlash[-p2 + qc]) . ((-I)*GS*DiracMatrix[li7]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 - q - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 + q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[q + qbbar, MB]*
  PropagatorDenominator[q + qb + qbbar, 0]*PropagatorDenominator[p2 - qc, MC]*
  PropagatorDenominator[-p2 + qc + qcbar, 0]*SUNF[Index[Gluon, 1], 
   Index[Gluon, 8], Index[Gluon, 9]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-p2 + q + qb]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[p2 - q - qb, MB]*PropagatorDenominator[
   p2 - q - qb - qbbar, 0]*PropagatorDenominator[p2 - q - qb - qbbar - qc, 
   MC]*SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[p2 - q - qbbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[p2 - q - qb - qbbar, 0]*
  PropagatorDenominator[-p2 + q + qbbar, MB]*PropagatorDenominator[
   p2 - q - qb - qbbar - qc, MC]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
   Index[Gluon, 8]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[q + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-q - qb - qbbar, 
    0]*PropagatorDenominator[-q - qb - qbbar - qc, MC]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + q + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[p2 - q - qb, MB]*
   PropagatorDenominator[p2 - q - qb - qbbar, 0]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[-p2 - q - qb - qbbar, li6]*
    MetricTensor[li2, li4] + FourVector[2*p2 - q - qb - qbbar, li4]*
    MetricTensor[li2, li6] + FourVector[-p2 + 2*(q + qb + qbbar), li2]*
    MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[
   p2 - q - qb - qbbar, 0]*PropagatorDenominator[q + qb + qbbar, 0]*
  PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[q + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[q + qbbar, 
    MB]*PropagatorDenominator[-q - qb - qbbar - qc, MC]*
   PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - q - qb - qbbar, 0]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[
    -p2 + q + qbbar, MB]*PropagatorDenominator[p2 - q - qb - qbbar - qc, 
    MC]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[-p2 + q + qb + qbbar + qc]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . DiracSpinor[-qcbar, MC]*
  MetricTensor[li4, li5]*(FourVector[-p2 - q - qb - qbbar, li6]*
    MetricTensor[li2, li4] + FourVector[2*p2 - q - qb - qbbar, li4]*
    MetricTensor[li2, li6] + FourVector[-p2 + 2*(q + qb + qbbar), li2]*
    MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[p2 - q - qb - qbbar, 0]*
  PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[q + qb + qbbar, 
   0]*PropagatorDenominator[p2 - q - qb - qbbar - qc, MC]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-p2 + qcbar, MC]*
  PropagatorDenominator[-p2 + qc + qcbar, 0]*PropagatorDenominator[
   -p2 + qbbar + qc + qcbar, MB]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qb - qc - qcbar, MB]*
  PropagatorDenominator[-p2 + qcbar, MC]*PropagatorDenominator[
   -p2 + qc + qcbar, 0]*PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 
   0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[q + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-q - qb - qbbar, 
    0]*PropagatorDenominator[-q - qb - qbbar - qc, MC]*
   PropagatorDenominator[-p2 + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[-p2 + qcbar, MC]*
   PropagatorDenominator[-p2 + qc + qcbar, 0]*PropagatorDenominator[
    -p2 + qbbar + qc + qcbar, MB]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 - q - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 + q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qb, MB]*
  PropagatorDenominator[q + qb + qbbar, 0]*PropagatorDenominator[-p2 + qcbar, 
   MC]*PropagatorDenominator[-p2 + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]], 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[q + qb + qbbar + qc]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MC + DiracSlash[p2 - qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb - qbbar, 0]*PropagatorDenominator[q + qbbar, 
    MB]*PropagatorDenominator[-q - qb - qbbar - qc, MC]*
   PropagatorDenominator[-p2 + qcbar, MC]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*
   DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[
    p2 - qb - qc - qcbar, MB]*PropagatorDenominator[-p2 + qcbar, MC]*
   PropagatorDenominator[-p2 + qc + qcbar, 0]), 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qb, MB] . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   (MC + DiracSlash[p2 - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . DiracSpinor[-qcbar, MC]*MetricTensor[li4, li5]*
  (FourVector[-p1 - q - qb - qbbar, li6]*MetricTensor[li1, li4] + 
   FourVector[p1 - p2 + qc + qcbar, li4]*MetricTensor[li1, li6] + 
   FourVector[p2 + q + qb + qbbar - qc - qcbar, li1]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[q + qbbar, MB]*
  PropagatorDenominator[q + qb + qbbar, 0]*PropagatorDenominator[-p2 + qcbar, 
   MC]*PropagatorDenominator[-p2 + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li7]*
     SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[-qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*(FourVector[-2*p2 + qb + qbbar + qc + qcbar, li6]*
     MetricTensor[li2, li5] + FourVector[p2 + qb + qbbar + qc + qcbar, li5]*
     MetricTensor[li2, li6] + FourVector[p2 - 2*(qb + qbbar + qc + qcbar), 
      li2]*MetricTensor[li5, li6])*MetricTensor[li6, li7]*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[qc + qcbar, 0]*
   PropagatorDenominator[qbbar + qc + qcbar, MB]*
   PropagatorDenominator[qb + qbbar + qc + qcbar, 0]*
   PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]]*
   SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li8]*
     SUNT[Index[Gluon, 10]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*(FourVector[-2*p2 + qb + qbbar + qc + qcbar, li6]*
     MetricTensor[li2, li5] + FourVector[p2 + qb + qbbar + qc + qcbar, li5]*
     MetricTensor[li2, li6] + FourVector[p2 - 2*(qb + qbbar + qc + qcbar), 
      li2]*MetricTensor[li5, li6])*MetricTensor[li6, li7]*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qc - qcbar, MB]*
   PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    qb + qbbar + qc + qcbar, 0]*PropagatorDenominator[
    -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
    Index[Gluon, 9]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[qc + qcbar, 0]*
  PropagatorDenominator[qbbar + qc + qcbar, MB]*
  PropagatorDenominator[-p2 + qbbar + qc + qcbar, MB]*
  PropagatorDenominator[-p2 + qb + qbbar + qc + qcbar, 0]*
  SUNF[Index[Gluon, 1], Index[Gluon, 7], Index[Gluon, 8]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-2*p2 + qc + qcbar, li8]*MetricTensor[li2, li6] + 
    FourVector[p2 + qc + qcbar, li6]*MetricTensor[li2, li8] + 
    FourVector[p2 - 2*(qc + qcbar), li2]*MetricTensor[li6, li8])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qc - qcbar, 0]*
   PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    -p2 + qbbar + qc + qcbar, MB]*PropagatorDenominator[
    -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 2], Index[Gluon, 9], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
   (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p1 - q, li4]*MetricTensor[li1, li3] + 
   FourVector[p1 - p2 + qb + qbbar + qc + qcbar, li3]*
    MetricTensor[li1, li4] + FourVector[p2 + q - qb - qbbar - qc - qcbar, 
     li1]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-qb - qc - qcbar, MB]*
  PropagatorDenominator[p2 - qb - qc - qcbar, MB]*
  PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
   -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
   Index[Gluon, 8]], -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li7]*
     SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[-p2 + qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p1 - q, li4]*
     MetricTensor[li1, li3] + FourVector[p1 - p2 + qb + qbbar + qc + qcbar, 
      li3]*MetricTensor[li1, li4] + 
    FourVector[p2 + q - qb - qbbar - qc - qcbar, li1]*MetricTensor[li3, li4])*
   MetricTensor[li4, li5]*MetricTensor[li6, li7]*
   (FourVector[-2*p2 + qc + qcbar, li8]*MetricTensor[li2, li6] + 
    FourVector[p2 + qc + qcbar, li6]*MetricTensor[li2, li8] + 
    FourVector[p2 - 2*(qc + qcbar), li2]*MetricTensor[li6, li8])*
   MetricTensor[li8, li9]*PolarizationVector[p1, li1]*
   PolarizationVector[p2, li2]*PropagatorDenominator[p2 - qc - qcbar, 0]*
   PropagatorDenominator[p2 - qb - qc - qcbar, MB]*
   PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    -p2 + qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 7], 
    Index[Gluon, 8]]*SUNF[Index[Gluon, 2], Index[Gluon, 9], 
    Index[Gluon, 10]]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li7]*
     SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[-qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li8]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p2 - q, li5]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li5] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li5])*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - q, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + qb + qbbar + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 + q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + q, 0]*PropagatorDenominator[qc + qcbar, 0]*
   PropagatorDenominator[qbbar + qc + qcbar, MB]*
   PropagatorDenominator[qb + qbbar + qc + qcbar, 0]*
   SUNF[Index[Gluon, 1], Index[Gluon, 8], Index[Gluon, 9]]*
   SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li8]*
     SUNT[Index[Gluon, 10]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p2 - q, li5]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li5] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li5])*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - q, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + qb + qbbar + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 + q - qb - qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-p2 + q, 0]*PropagatorDenominator[-qb - qc - qcbar, 
    MB]*PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    qb + qbbar + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
    Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
    Index[Gluon, 8]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-p2 + q + qb]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[-qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li6]*
    SUNT[Index[Gluon, 9]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[p2 - q - qb, MB]*PropagatorDenominator[qc + qcbar, 0]*
  PropagatorDenominator[qbbar + qc + qcbar, MB]*
  SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li6]*SUNT[Index[Gluon, 9]]) . 
   (MB + DiracSlash[qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
    SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[p2 - q - qbbar]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  (FourVector[-p2 - q, li4]*MetricTensor[li2, li3] + 
   FourVector[2*p2 - q, li3]*MetricTensor[li2, li4] + 
   FourVector[-p2 + 2*q, li2]*MetricTensor[li3, li4])*MetricTensor[li4, li5]*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[p2 - q, 0]*
  PropagatorDenominator[-p2 + q + qbbar, MB]*PropagatorDenominator[
   -qb - qc - qcbar, MB]*PropagatorDenominator[qc + qcbar, 0]*
  SUNF[Index[Gluon, 2], Index[Gluon, 7], Index[Gluon, 8]], 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[p2 - q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p2 - q, li8]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li8] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li8])*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - q - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 + q + qb + qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - q, 0]*PropagatorDenominator[-p2 + q + qbbar, 
    MB]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], 
    Index[Gluon, 8], Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
    Index[Gluon, 10]]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li9]*
     SUNT[Index[Gluon, 10]]) . (MB + DiracSlash[-p2 + q + qb]) . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*(FourVector[-p2 - q, li8]*
     MetricTensor[li2, li3] + FourVector[2*p2 - q, li3]*
     MetricTensor[li2, li8] + FourVector[-p2 + 2*q, li2]*
     MetricTensor[li3, li8])*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - q - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 + q + qb + qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[p2 - q, 0]*PropagatorDenominator[p2 - q - qb, MB]*
   PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], 
    Index[Gluon, 8], Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 7], 
    Index[Gluon, 10]]), -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[qc + qcbar, 0]*
   PropagatorDenominator[qbbar + qc + qcbar, MB]*
   PropagatorDenominator[-p2 + qbbar + qc + qcbar, MB]), 
 -(Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + q + qb]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[-qbbar - qc - qcbar]) . 
    ((-I)*GS*DiracMatrix[li4]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[p2 - q - qb, MB]*
   PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    qbbar + qc + qcbar, MB]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - qc - qcbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + q + qb + qbbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 - q - qb - qbbar + qc + qcbar, li1]*
     MetricTensor[li4, li6])*(FourVector[-2*p2 + qc + qcbar, li8]*
     MetricTensor[li2, li5] + FourVector[p2 + qc + qcbar, li5]*
     MetricTensor[li2, li8] + FourVector[p2 - 2*(qc + qcbar), li2]*
     MetricTensor[li5, li8])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[q + qb + qbbar, 
    0]*PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    -p2 + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
    Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
    Index[Gluon, 10]]), -(GS^2*Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
     SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - q - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 + q + qb + qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*(FourVector[-2*p2 + q + qb + qbbar, li8]*
     MetricTensor[li2, li5] + FourVector[p2 + q + qb + qbbar, li5]*
     MetricTensor[li2, li8] + FourVector[p2 - 2*(q + qb + qbbar), li2]*
     MetricTensor[li5, li8])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[q + qb + qbbar, 
    0]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], 
    Index[Gluon, 8], Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . (MB + DiracSlash[q + qb]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[p2 - qbbar - qc - qcbar]) . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
  (FourVector[-2*p2 + qc + qcbar, li6]*MetricTensor[li2, li4] + 
   FourVector[p2 + qc + qcbar, li4]*MetricTensor[li2, li6] + 
   FourVector[p2 - 2*(qc + qcbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[-q - qb, MB]*
  PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[qc + qcbar, 
   0]*PropagatorDenominator[-p2 + qbbar + qc + qcbar, MB]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
   (MB + DiracSlash[q + qb]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-p2 + q + qb]) . 
   ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li4, li5]*(FourVector[-p1 + p2 - q - qb - qbbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 + qc + qcbar, li4]*
    MetricTensor[li1, li6] + FourVector[-p2 + q + qb + qbbar - qc - qcbar, 
     li1]*MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[-q - qb, MB]*PropagatorDenominator[p2 - q - qb, MB]*
  PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
  PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]], -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
    (MB + DiracSlash[p2 - q - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
     SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[
    -p2 + q + qbbar, MB]*PropagatorDenominator[-qb - qc - qcbar, MB]*
   PropagatorDenominator[qc + qcbar, 0]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li9]*SUNT[Index[Gluon, 10]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li7]*
     SUNT[Index[Gluon, 9]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - qc - qcbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + q + qb + qbbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 - q - qb - qbbar + qc + qcbar, li1]*
     MetricTensor[li4, li6])*(FourVector[-2*p2 + qc + qcbar, li8]*
     MetricTensor[li2, li5] + FourVector[p2 + qc + qcbar, li5]*
     MetricTensor[li2, li8] + FourVector[p2 - 2*(qc + qcbar), li2]*
     MetricTensor[li5, li8])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[q + qb + qbbar, 
    0]*PropagatorDenominator[qc + qcbar, 0]*PropagatorDenominator[
    -p2 + qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
    Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
    Index[Gluon, 10]]), -(Conjugate[PolarizationVector[q, li3]]*
   DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li4]*
     SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[qb + qc + qcbar]) . 
    ((-I)*GS*DiracMatrix[li2]*SUNT[Index[Gluon, 2]]) . 
    (MB + DiracSlash[-p2 + qb + qc + qcbar]) . ((-I)*GS*DiracMatrix[li1]*
     SUNT[Index[Gluon, 1]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[
    -qb - qc - qcbar, MB]*PropagatorDenominator[p2 - qb - qc - qcbar, MB]*
   PropagatorDenominator[qc + qcbar, 0]), 
 -(GS^2*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
    ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
    DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li9]*
     SUNT[Index[Gluon, 10]]) . (MB + DiracSlash[-q - qbbar]) . 
    ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . 
    DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
   (FourVector[-p1 + p2 - q - qb - qbbar, li6]*MetricTensor[li1, li4] + 
    FourVector[p1 + qc + qcbar, li4]*MetricTensor[li1, li6] + 
    FourVector[-p2 + q + qb + qbbar - qc - qcbar, li1]*
     MetricTensor[li4, li6])*(FourVector[-2*p2 + q + qb + qbbar, li8]*
     MetricTensor[li2, li5] + FourVector[p2 + q + qb + qbbar, li5]*
     MetricTensor[li2, li8] + FourVector[p2 - 2*(q + qb + qbbar), li2]*
     MetricTensor[li5, li8])*MetricTensor[li6, li7]*MetricTensor[li8, li9]*
   PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
   PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[q + qb + qbbar, 
    0]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
   PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], 
    Index[Gluon, 8], Index[Gluon, 9]]*SUNF[Index[Gluon, 2], Index[Gluon, 8], 
    Index[Gluon, 10]]), GS*Conjugate[PolarizationVector[q, li3]]*
  DiracSpinor[qc, MC] . ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . 
   DiracSpinor[-qcbar, MC]*DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*
    SUNT[Index[Gluon, 8]]) . (MB + DiracSlash[-p2 + qb + qc + qcbar]) . 
   ((-I)*GS*DiracMatrix[li1]*SUNT[Index[Gluon, 1]]) . 
   (MB + DiracSlash[-q - qbbar]) . ((-I)*GS*DiracMatrix[li3]*
    SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*MetricTensor[li4, li5]*
  (FourVector[-2*p2 + qc + qcbar, li6]*MetricTensor[li2, li4] + 
   FourVector[p2 + qc + qcbar, li4]*MetricTensor[li2, li6] + 
   FourVector[p2 - 2*(qc + qcbar), li2]*MetricTensor[li4, li6])*
  MetricTensor[li6, li7]*PolarizationVector[p1, li1]*
  PolarizationVector[p2, li2]*PropagatorDenominator[q + qbbar, MB]*
  PropagatorDenominator[p2 - qc - qcbar, 0]*PropagatorDenominator[
   p2 - qb - qc - qcbar, MB]*PropagatorDenominator[qc + qcbar, 0]*
  SUNF[Index[Gluon, 2], Index[Gluon, 8], Index[Gluon, 9]], 
 GS*Conjugate[PolarizationVector[q, li3]]*DiracSpinor[qc, MC] . 
   ((-I)*GS*DiracMatrix[li7]*SUNT[Index[Gluon, 9]]) . DiracSpinor[-qcbar, MC]*
  DiracSpinor[qb, MB] . ((-I)*GS*DiracMatrix[li5]*SUNT[Index[Gluon, 8]]) . 
   (MB + DiracSlash[p2 - q - qbbar]) . ((-I)*GS*DiracMatrix[li2]*
    SUNT[Index[Gluon, 2]]) . (MB + DiracSlash[-q - qbbar]) . 
   ((-I)*GS*DiracMatrix[li3]*SUNT[Index[Gluon, 7]]) . DiracSpinor[-qbbar, MB]*
  MetricTensor[li4, li5]*(FourVector[-p1 + p2 - q - qb - qbbar, li6]*
    MetricTensor[li1, li4] + FourVector[p1 + qc + qcbar, li4]*
    MetricTensor[li1, li6] + FourVector[-p2 + q + qb + qbbar - qc - qcbar, 
     li1]*MetricTensor[li4, li6])*MetricTensor[li6, li7]*
  PolarizationVector[p1, li1]*PolarizationVector[p2, li2]*
  PropagatorDenominator[q + qbbar, MB]*PropagatorDenominator[-p2 + q + qbbar, 
   MB]*PropagatorDenominator[-p2 + q + qb + qbbar, 0]*
  PropagatorDenominator[qc + qcbar, 0]*SUNF[Index[Gluon, 1], Index[Gluon, 8], 
   Index[Gluon, 9]]}
